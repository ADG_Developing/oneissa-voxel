#include <glm/glm.hpp>

class Block
{
public:
	Block();
	~Block();

	void init(int ID, int texTopX, int texTopY, int texBottomX, int texBottomY, int texSideX, int texSideY);
	void addLiquid(int x, int y, int z);
	void initLiquid();

	float updateLiquid(bool bx[2], bool by[2], bool bz[2]);

	int blockID;
	int texCoordTopX;
	int texCoordTopY;
	int texCoordBottomX;
	int texCoordBottomY;
	int texCoordSideX;
	int texCoordSideY;

	float blockHeight = 0;
	float liquidLevel = 0;

	int liquidLevels[100];

	int lX = 0;
	int lY = 0;
	int lZ = 0;

	bool b1 = false;
	bool b2 = true;

	glm::vec3 posLiquidP;
	glm::vec3 negLiquidP;
private:
};