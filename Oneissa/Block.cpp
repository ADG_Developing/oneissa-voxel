#include "Block.h"

#include <iostream>

Block::Block()
{

}

Block::~Block()
{

}

void Block::init(int ID, int texTopX, int texTopY, int texBottomX, int texBottomY, int texSideX, int texSideY)
{
	blockID = ID;

	texCoordTopX = texTopX;
	texCoordTopY = texTopY;
	texCoordBottomX = texBottomX;
	texCoordBottomY = texBottomY;
	texCoordSideX = texSideX;
	texCoordSideY = texSideY;
}

void Block::addLiquid(int x, int y, int z)
{
	liquidLevel = 100;

	lX = x;
	lY = y;
	lZ = z;

	b2 = false;
}

void Block::initLiquid()
{
	//blockHeight = 1.00f / liquidLevel;
}

float Block::updateLiquid(bool bx[2], bool by[2], bool bz[2])
{
	blockHeight = 1.00f - (0.01f * liquidLevel);
	liquidLevel--;

	return 0;
}