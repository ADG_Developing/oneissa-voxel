#include <glm/glm.hpp>
#include <SDL.h>
#include <iostream>

using namespace std;

class Camera;
class CoordSystem;
class Chunk;
class ChunkManager;

class PhysicsEngine
{
public:
	PhysicsEngine(Camera* cam, ChunkManager* cman, CoordSystem* coordsystem);
	~PhysicsEngine();

	glm::vec3 getRaycastPosition();
	glm::vec3 getRaycastPositionBlock();
	Chunk* getChunk();

	void updateGravVelocity(int groundHeight);

	float gravity = 0.01f;
private:
	Camera* _camera;
	CoordSystem* _coordSys;
	ChunkManager* _cManager;

	glm::vec3 velocity = glm::vec3(0, 0, 0);

	float moveSpeed = 0.2f;
	float jumpSpeed = 0.25f;
};