#include "ChunkManager.h"

#include "CoordSystem.h"
#include "Chunk.h"
#include "Player.h"
#include "Camera.h"
#include "VBO.h"
#include "GLSLShader.h"
#include "OneissaMath.h"
#include "ContentSystem.h"
#include "BlockManager.h"
#include "Block.h"

ChunkManager::ChunkManager(Player* p, CoordSystem *coordsys, Camera *camera, OneissaMath *omath, ContentSystem *contsystem, BlockManager* bMan)
{
	_player = p;
	_coordsystem = coordsys;
	_cam = camera;
	_oneissaMath = omath;
	_contentSystem = contsystem;
	_bManager = bMan;

	sManager = new GLSLShader();
}

ChunkManager::~ChunkManager()
{

}

void ChunkManager::init()
{
	matrix.resize(dequeWidth);

	for (int i = 0; i < dequeWidth; i++) {
		matrix[i].resize(dequeWidth);
		for (int j = 0; j < dequeWidth; j++) {
			matrix[i][j].resize(dequeWidth, 128.0f);
		}
	}
}

void ChunkManager::buildWorld()
{
	cubeShader = sManager->LoadShader("../data/shaders/Cube.vert", "../data/shaders/Cube.frag");
	glUseProgram(cubeShader);

	int chunkView = 10;

	for (int x = 0; x < chunkView; x++)
	{
		for (int y = 0; y < chunkView; y++)
		{
			for (int z = 0; z < chunkView; z++)
			{
				//if (x > 0 && y > 0 && z > 0)
				//{
					chunkList[(x * 32)][(y * 32)][(z * 32)] = new Chunk(_coordsystem, this, _cam, _oneissaMath, _contentSystem, _bManager);
					chunkList[(x * 32)][(y * 32)][(z * 32)]->init();
					chunkList[(x * 32)][(y * 32)][(z * 32)]->fillChunk((x * 32), (y * 32), (z * 32));
					chunkList[(x * 32)][(y * 32)][(z * 32)]->buildChunk((x * 32), (y * 32), (z * 32));
					voxelCount += chunkList[(x * 32)][(y * 32)][(z * 32)]->voxelCount;
					chunkCount++;
				//}
			}
		}
	}

	std::cout << voxelCount << endl;
	std::cout << chunkCount << endl;
}

void ChunkManager::renderWorld()
{
	int chunkView = 10;

	for (int x = 0; x < chunkView; x++)
	{
		for (int y = 0; y < chunkView; y++)
		{
			for (int z = 0; z < chunkView; z++)
			{
				//if (x > 0 && y > 0 && z > 0)
				//{
					if (chunkList[(x * 32)][(y * 32)][(z * 32)]->_vbo->cubeVerts.empty())
					{
						chunkList[(x * 32)][(y * 32)][(z * 32)] == NULL;
					}
					else
					{
						VBO *vbo = chunkList[(x * 32)][(y * 32)][(z * 32)]->_vbo;

						vbo->renderVoxels();
						vbo->shader(cubeShader, 0, _cam);
					}
				//}
			}
		}
	}
}

void ChunkManager::loadChunk(float x, float y, float z)
{
	
}

void ChunkManager::unloadChunk(float x, float y, float z)
{

}

Chunk* ChunkManager::getChunk(float x, float y, float z)
{
	return chunkList[(int)x][(int)y][(int)z];
}

void ChunkManager::addChunk(float x, float y, float z)
{

}