#include "GameManager.h"
#include "Player.h"
#include "ChunkManager.h"
#include "ContentSystem.h"
#include "OneissaMath.h"
#include "coordSystem.h"
#include "GLSLShader.h"
#include "Chunk.h"
#include "VBO.h"
#include "Camera.h"
#include "GlobalStructs.h"
#include "PhysicsEngine.h"
#include "TextManager.h"
#include "BlockManager.h"
#include "Block.h"

#include <stdio.h>

GLuint cubeShader;
GLuint cubeShader2;

GameManager::GameManager(Camera *c)
{
	_camera = c;

	bManager = new BlockManager();
	bManager->init();

	cSystem = new ContentSystem();
	oMath = new OneissaMath();
	coordSystem = new CoordSystem();
	player = new Player(_camera);
	cManager = new ChunkManager(player, coordSystem, _camera, oMath, cSystem, bManager);
	physicsEngine = new PhysicsEngine(_camera, cManager, coordSystem);
	textManager = new TextManager();
}

GameManager::~GameManager()
{

}

glm::vec3 getChunkByMousePos(glm::vec3 mousePos)
{
	glm::ivec3 global = (glm::ivec3)mousePos;
	glm::ivec3 chunkSize = glm::ivec3(32, 32, 32);

	//local = glm::ivec3(((global.x % chunkSize.x) + chunkSize.x) % chunkSize.x, ((global.y % chunkSize.y) + chunkSize.y) % chunkSize.y, ((global.x % chunkSize.z) + chunkSize.z) % chunkSize.z);

	float x = ((int)global.x - ((int)global.x % chunkSize.x)) / chunkSize.x;
	float y = ((int)global.y - ((int)global.y % chunkSize.y)) / chunkSize.y;
	float z = ((int)global.z - ((int)global.z % chunkSize.z)) / chunkSize.z;

	cout << global.x << ", " << y << ", " << z << endl;

	return glm::vec3(x, y, z);
}

void GameManager::addBlock2(int x, int y, int z, int type, float height)
{
	glm::vec3 pos = glm::vec3(x, y, z);
	glm::vec3 currChunkPos = getChunkByMousePos(pos);

	pos.x = floor(pos.x);
	pos.y = floor(pos.y);
	pos.z = floor(pos.z);

	Chunk* currChunk;

	if (cManager->getChunk(currChunkPos.x * 32, currChunkPos.y * 32, currChunkPos.z * 32) != NULL)
	{
		currChunk = cManager->getChunk(currChunkPos.x * 32, currChunkPos.y * 32, currChunkPos.z * 32);

		coordSystem->addCoords(pos.x, pos.y, pos.z, type);

		VBO* currVBO = currChunk->_vbo;

		if (coordSystem->getBlock(pos.x, pos.y, pos.z).x != NULL)
		{
			float sizeX = 1;
			float sizeY = height;
			float sizeZ = 1;

			if (coordSystem->getBlock(pos.x - 1, pos.y, pos.z).x == 0)
			{
				currVBO->addVerts(pos.x, pos.y, pos.z, 3, sizeX, sizeY, sizeZ);
				currVBO->addIndices2();
				currVBO->addNormals(3);
				currVBO->addUV(3, bManager->water);

				currChunk->voxelCount += 0.16666666666f;
			}

			if (coordSystem->getBlock(pos.x + 1, pos.y, pos.z).x == 0)
			{
				currVBO->addVerts(pos.x, pos.y, pos.z, 4, sizeX, sizeY, sizeZ);
				currVBO->addIndices2();
				currVBO->addNormals(4);
				currVBO->addUV(4, bManager->water);

				currChunk->voxelCount += 0.16666666666f;
			}

			if (coordSystem->getBlock(pos.x, pos.y - 1, pos.z).x == 0)
			{
				currVBO->addVerts(pos.x, pos.y, pos.z, 2, sizeX, sizeY, sizeZ);
				currVBO->addIndices2();
				currVBO->addNormals(2);
				currVBO->addUV(2, bManager->water);

				currChunk->voxelCount += 0.16666666666f;
			}

			if (coordSystem->getBlock(pos.x, pos.y + 1, pos.z).x == 0)
			{
				currVBO->addVerts(pos.x, pos.y, pos.z, 1, sizeX, sizeY, sizeZ);
				currVBO->addIndices2();
				currVBO->addNormals(1);
				currVBO->addUV(1, bManager->water);

				currChunk->voxelCount += 0.16666666666f;
			}

			if (coordSystem->getBlock(pos.x, pos.y, pos.z - 1).x == 0)
			{
				currVBO->addVerts(pos.x, pos.y, pos.z, 6, sizeX, sizeY, sizeZ);
				currVBO->addIndices2();
				currVBO->addNormals(6);
				currVBO->addUV(6, bManager->water);

				currChunk->voxelCount += 0.16666666666f;
			}

			if (coordSystem->getBlock(pos.x, pos.y, pos.z + 1).x == 0)
			{
				currVBO->addVerts(pos.x, pos.y, pos.z, 5, sizeX, sizeY, sizeZ);
				currVBO->addIndices2();
				currVBO->addNormals(5);
				currVBO->addUV(5, bManager->water);

				currChunk->voxelCount += 0.16666666666f;
			}
		}

		currVBO->buildVBO();
	}
}

void GameManager::addBlock(int type, float height)
{
	glm::vec3 pos = physicsEngine->getRaycastPositionBlock();
	glm::vec3 currChunkPos = getChunkByMousePos(pos);

	pos.x = floor(pos.x);
	pos.y = floor(pos.y);
	pos.z = floor(pos.z);

	Chunk* currChunk;

	if (cManager->getChunk(currChunkPos.x * 32, currChunkPos.y * 32, currChunkPos.z * 32) != NULL)
	{
		currChunk = cManager->getChunk(currChunkPos.x * 32, currChunkPos.y * 32, currChunkPos.z * 32);

		if (type = 3)
		{
			bManager->water->addLiquid(pos.x, pos.y, pos.z);
			bManager->water->initLiquid();
		}

		coordSystem->addCoords(pos.x, pos.y, pos.z, type);

		VBO* currVBO = currChunk->_vbo;

		if (coordSystem->getBlock(pos.x, pos.y, pos.z).x != NULL)
		{
			int sizeX = 1;
			int sizeY = height;
			int sizeZ = 1;

			if (coordSystem->getBlock(pos.x - 1, pos.y, pos.z).x == 0)
			{
				currVBO->addVerts(pos.x, pos.y, pos.z, 3, sizeX, sizeY, sizeZ);
				currVBO->addIndices2();
				currVBO->addNormals(3);
				currVBO->addUV(3, bManager->water);

				currChunk->voxelCount += 0.16666666666f;
			}

			if (coordSystem->getBlock(pos.x + 1, pos.y, pos.z).x == 0)
			{
				currVBO->addVerts(pos.x, pos.y, pos.z, 4, sizeX, sizeY, sizeZ);
				currVBO->addIndices2();
				currVBO->addNormals(4);
				currVBO->addUV(4, bManager->water);

				currChunk->voxelCount += 0.16666666666f;
			}

			if (coordSystem->getBlock(pos.x, pos.y - 1, pos.z).x == 0)
			{
				currVBO->addVerts(pos.x, pos.y, pos.z, 2, sizeX, sizeY, sizeZ);
				currVBO->addIndices2();
				currVBO->addNormals(2);
				currVBO->addUV(2, bManager->water);

				currChunk->voxelCount += 0.16666666666f;
			}

			if (coordSystem->getBlock(pos.x, pos.y + 1, pos.z).x == 0)
			{
				currVBO->addVerts(pos.x, pos.y, pos.z, 1, sizeX, sizeY, sizeZ);
				currVBO->addIndices2();
				currVBO->addNormals(1);
				currVBO->addUV(1, bManager->water);

				currChunk->voxelCount += 0.16666666666f;
			}

			if (coordSystem->getBlock(pos.x, pos.y, pos.z - 1).x == 0)
			{
				currVBO->addVerts(pos.x, pos.y, pos.z, 6, sizeX, sizeY, sizeZ);
				currVBO->addIndices2();
				currVBO->addNormals(6);
				currVBO->addUV(6, bManager->water);

				currChunk->voxelCount += 0.16666666666f;
			}

			if (coordSystem->getBlock(pos.x, pos.y, pos.z + 1).x == 0)
			{
				currVBO->addVerts(pos.x, pos.y, pos.z, 5, sizeX, sizeY, sizeZ);
				currVBO->addIndices2();
				currVBO->addNormals(5);
				currVBO->addUV(5, bManager->water);

				currChunk->voxelCount += 0.16666666666f;
			}
		}

		currVBO->buildVBO();
	}
}

void GameManager::removeBlock2(int x, int y, int z)
{
	glm::vec3 pos = glm::vec3(x, y, z);
	glm::vec3 currChunkPos = getChunkByMousePos(pos);

	Chunk* currChunk;

	if (cManager->getChunk(currChunkPos.x * 32, currChunkPos.y * 32, currChunkPos.z * 32) != NULL)
	{
		currChunk = cManager->getChunk(currChunkPos.x * 32, currChunkPos.y * 32, currChunkPos.z * 32);

		std::cout << currChunk->voxelCount << endl;

		VBO* currVBO = currChunk->_vbo;

		coordSystem->removeCoords(pos.x, pos.y, pos.z);

		//currVBO->cubeVerts.clear();
		//currVBO->cubeUVs.clear();
		//currVBO->cubeNormals.clear();

		//currChunk->rebuildChunk(currChunkPos.x, currChunkPos.y, currChunkPos.z);
	}
}

void GameManager::removeBlock()
{
	glm::vec3 pos = physicsEngine->getRaycastPosition();
	glm::vec3 currChunkPos = getChunkByMousePos(pos);

	Chunk* currChunk;

	if (cManager->getChunk(currChunkPos.x * 32, currChunkPos.y * 32, currChunkPos.z * 32) != NULL)
	{
		currChunk = cManager->getChunk(currChunkPos.x * 32, currChunkPos.y * 32, currChunkPos.z * 32);

		VBO* currVBO = currChunk->_vbo;

		coordSystem->removeCoords(pos.x, pos.y, pos.z);

		//currVBO->cubeVerts.clear();
		//currVBO->cubeUVs.clear();
		//currVBO->cubeNormals.clear();

		//currChunk->rebuildChunk(currChunkPos.x, currChunkPos.y, currChunkPos.z);
	}
}

int GameManager::getBlock(glm::vec3 pos)
{
	return coordSystem->getBlock(pos.x, pos.y, pos.z).x;
}

void GameManager::setBlock(glm::vec3 pos, int type)
{
	coordSystem->addCoords(pos.x, pos.y, pos.z, type);
}

int i = 0;
int shader = 0;
GLuint TextureID = 0;
SDL_Surface * surface;

void GameManager::init(SDL_Renderer* renderer)
{
	shaderManager = new GLSLShader();

	cManager->init();

	font = TTF_OpenFont("../data/fonts/XTemplate_font_stencil.ttf", 25);

	_camera->setPosition(100, 100, 100);

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

void GameManager::build()
{
	cManager->buildWorld();
}

int groundHeight = 0;

void GameManager::loop(SDL_Renderer* renderer)
{
	//bool xb[2];
	//bool yb[2];
	//bool zb[2];

	//if (coordSystem->getBlock(bManager->water->lX + 1, bManager->water->lY, bManager->water->lZ).x != NULL)
	//{
	//	xb[0] = true;
	//}
	//else
	//{
	//	xb[0] = false;
	//}

	//if (bManager->water->lX - 1 > 0 && coordSystem->getBlock(bManager->water->lX - 1, bManager->water->lY, bManager->water->lZ).x != NULL)
	//{
	//	xb[1] = true;
	//}
	//else
	//{
	//	xb[1] = false;
	//}

	//if (bManager->water->lY - 1 > 0 && coordSystem->getBlock(bManager->water->lX, bManager->water->lY - 1, bManager->water->lZ).x != NULL)
	//{
	//	yb[1] = true;
	//}
	//else
	//{
	//	yb[1] = false;
	//}

	//if (coordSystem->getBlock(bManager->water->lX, bManager->water->lY, bManager->water->lZ + 1).x != NULL)
	//{
	//	zb[0] = true;
	//}
	//else
	//{
	//	zb[0] = false;
	//}

	//if (bManager->water->lZ - 1 > 0 && coordSystem->getBlock(bManager->water->lX, bManager->water->lY, bManager->water->lZ - 1).x != NULL)
	//{
	//	zb[1] = true;
	//}
	//else
	//{
	//	zb[1] = false;
	//}

	//if (bManager->water->liquidLevel > 1)
	//{
	//	if (!bManager->water->b1)
	//	{
	//		if (!bManager->water->b2)
	//		{
	//			addBlock2(bManager->water->updateLiquidXP(xb, yb, zb), bManager->water->lY, bManager->water->lZ, 3, bManager->water->blockHeight);
	//			addBlock2(bManager->water->updateLiquidXN(xb, yb, zb), bManager->water->lY, bManager->water->lZ, 3, bManager->water->blockHeight);
	//			addBlock2(bManager->water->lX, bManager->water->lY, bManager->water->updateLiquidZP(xb, yb, zb), 3, bManager->water->blockHeight);
	//			addBlock2(bManager->water->lX, bManager->water->lY, bManager->water->updateLiquidZN(xb, yb, zb), 3, bManager->water->blockHeight);
	//		}
	//	}
	//}

	//glm::vec3 pos = glm::vec3(_camera->getPosition().x, _camera->getPosition().y, _camera->getPosition().z);
	//glm::vec3 currChunkPos = getChunkByMousePos(pos);

	//Chunk* currChunk = (Chunk*)NULL;

	//if (cManager->getChunk(currChunkPos.x * 32, currChunkPos.y * 32, currChunkPos.z * 32) != NULL)
	//{
	//	currChunk = cManager->getChunk(currChunkPos.x * 32, currChunkPos.y * 32, currChunkPos.z * 32);

	//	if (_camera->getPosition().y - 4 > 0 && currChunk->getBlock(_camera->getPosition().x, _camera->getPosition().y - 4, _camera->getPosition().z) != NULL)
	//	{
	//		groundHeight = _camera->getPosition().y - 4;
	//	}
	//	else
	//	{
	//		groundHeight = 0;
	//	}
	//}

	if (_camera->getPosition().y - 4 > 0 && coordSystem->getBlock(_camera->getPosition().x, _camera->getPosition().y - 4, _camera->getPosition().z).x != NULL)
	{
		groundHeight = _camera->getPosition().y - 4;
	}
	else
	{
		groundHeight = -99999999999999999;
	}

	float moveSpeed = 0.1f;
	bool isNoclip;

	isNoclip = true;

	if (isNoclip)
	{
		float moveSpeed = 0.3f;

		if (Keys[SDLK_w].pr){
			_camera->_position += _camera->_direction*moveSpeed;
		}
		else if (Keys[SDLK_s].pr){
			_camera->_position -= _camera->_direction*moveSpeed;
		}

		if (Keys[SDLK_d].pr){
			_camera->_position += _camera->_right*moveSpeed;
		}
		else if (Keys[SDLK_a].pr){
			_camera->_position -= _camera->_right*moveSpeed;
		}

		if (Keys[SDLK_SPACE].pr){
			_camera->_position += _camera->_up*moveSpeed;
		}
		else if (Keys[SDLK_LSHIFT].pr){
			_camera->_position -= _camera->_up*moveSpeed;
		}
	}
	else
	{
		glm::vec3 xzDirection = glm::normalize(glm::vec3(_camera->_direction.x, 0, _camera->_direction.z));
		glm::vec3 xzDirectionRight = glm::normalize(glm::vec3(_camera->_right.x, 0, _camera->_right.z));

		if (Keys[SDLK_w].pr){
			if (coordSystem->getBlock(_camera->getPosition().x, _camera->getPosition().y - 3, _camera->getPosition().z + 2).x != NULL)
			{
				_camera->_position += xzDirection*0.0f;
				//_camera->_position += _camera->_up*(moveSpeed * 4);
			}
			else
			{
				_camera->_position += xzDirection*moveSpeed;
			}
		}
		else if (Keys[SDLK_s].pr){
			if (_camera->getPosition().x - 2 >= 0 && coordSystem->getBlock(_camera->getPosition().x, _camera->getPosition().y - 3, _camera->getPosition().z - 2).x != NULL)
			{
				_camera->_position -= xzDirection*0.0f;
				//_camera->_position += _camera->_up*(moveSpeed * 4);
			}
			else
			{
				_camera->_position -= xzDirection*moveSpeed;
			}
		}

		if (Keys[SDLK_d].pr){
			if (coordSystem->getBlock(_camera->getPosition().x + 2, _camera->getPosition().y - 3, _camera->getPosition().z).x != NULL)
			{
				_camera->_position += xzDirectionRight*0.0f;
				//_camera->_position += _camera->_up*(moveSpeed * 4);
			}
			else
			{
				_camera->_position += xzDirectionRight*moveSpeed;
			}
		}
		else if (Keys[SDLK_a].pr){
			if (_camera->getPosition().x - 2 >= 0 && coordSystem->getBlock(_camera->getPosition().x - 2, _camera->getPosition().y - 3, _camera->getPosition().z).x != NULL)
			{
				_camera->_position -= xzDirectionRight*0.0f;
				//_camera->_position += _camera->_up*(moveSpeed * 4);
			}
			else
			{
				_camera->_position -= xzDirectionRight*moveSpeed;
			}
		}

		physicsEngine->updateGravVelocity(groundHeight);
	}

	cManager->renderWorld();

	//SDL_Texture *textTexture;
	//char buf[256];
	//SDL_Color foregroundColor = { 255, 255, 255 };
	//SDL_Color backgroundColor = { 0, 0, 0, 0};
	//SDL_Rect TextLocation;


	//sprintf(buf, "TESTING");
	//SDL_Surface *textSurface = TTF_RenderText_Shaded(font, buf,
	//	foregroundColor, backgroundColor);

	////if (textSurface)
	////{
	//	textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
	//	TextLocation.h = textSurface->h;
	//	TextLocation.w = textSurface->w;
	//	TextLocation.x = 100;
	//	TextLocation.y = 100;
	////}

	////free surface 
	//SDL_FreeSurface(textSurface);

	//SDL_RenderCopy(renderer, textTexture, NULL, &TextLocation);
	//SDL_RenderPresent(renderer);
}