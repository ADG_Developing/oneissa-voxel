#include "ContentSystem.h"

ContentSystem::ContentSystem()
{

}

ContentSystem::~ContentSystem()
{

}

GLuint ContentSystem::loadPNG(int id, char* path)
{
	texture[id] = SOIL_load_OGL_texture
	(
	path,
	SOIL_LOAD_AUTO,
	SOIL_CREATE_NEW_ID,
	SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_LOAD_RGBA | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
	);

	//GLuint TextureID = 0;

	//SDL_Surface* Surface = IMG_Load(path);

	//glGenTextures(1, &TextureID);
	//glBindTexture(GL_TEXTURE_2D, TextureID);

	//int Mode = GL_RGB;

	//if (Surface->format->BytesPerPixel == 4) {
	//	Mode = GL_RGBA;
	//}

	//glTexImage2D(GL_TEXTURE_2D, 0, Mode, Surface->w, Surface->h, 0, Mode, GL_UNSIGNED_BYTE, Surface->pixels);

	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//texture[id] = TextureID;
	return texture[id];
}

TTF_Font* ContentSystem::loadfont(char* file, int ptsize)
{
	TTF_Font* tmpfont;
	tmpfont = TTF_OpenFont(file, ptsize);
	if (tmpfont == NULL){
		printf("Unable to load font: %s %s \n", file, TTF_GetError());
		// Handle the error here.
	}
	return tmpfont;
}