#include "Chunk.h"

#include "CoordSystem.h"
#include "ChunkManager.h"
#include "VBO.h"
#include "GLSLShader.h"
#include "OneissaMath.h"
#include "Noise.h"
#include "BlockManager.h"
#include "Block.h"

Chunk::Chunk(CoordSystem *_coordsys, ChunkManager *_cman, Camera *_camera, OneissaMath *_omath, ContentSystem *_contsystem, BlockManager* _bMan)
{
	_csystem = _coordsys;
	_cam = _camera;
	_oneissaMath = _omath;
	_contentSystem = _contsystem;
	_bManager = _bMan;

	_vbo = new VBO(_omath, _contsystem, _bManager);

	init();
}

Chunk::~Chunk()
{

}

void Chunk::init()
{
	srand(time(0));
}

//void Chunk::addBlock(int x, int y, int z, Block* block)
//{
//	cubeCoords[x][y][z] = block->blockID;
//}
//
//Block* Chunk::_csystem->getBlock(int x, int y, int z)
//{
//	return _bManager->getBlockByID(cubeCoords[x][y][z]);
//}

double test(double x, double y, double startFrequence, int octaveCount, double persistence)
{
	double noise = 0;
	double normalizeFactor = 0;

	double frequence = startFrequence;
	double amplitude = 1;

	for (int i = 0; i < octaveCount; i++)
	{
		normalizeFactor += amplitude;

		noise += amplitude * 1;

		frequence *= 2;

		amplitude *= persistence;
	}

	return noise / normalizeFactor;
}

void Chunk::test(int x, int y, int z, int i, int j, int k)
{
	//float randomFloat = (rand() % (6 - 4)) + 4;
	//float randomFloat2 = (rand() % (10 - 5)) + 5;

	float seed = 1541; // 1441

	float randomFloat = 6.f;
	float randomFloat2 = 5.f;

	float h = (256.f * (float)Noise::noise2dPerlin(
		x + i, z + k, seed, randomFloat, randomFloat2 / 10, 256)) - 46;

	if (h >= j)
		h -= j;
	else
		h = 0;

	//if (h < 0)

	if (h != 0)
	{
		for (float y = 0; y < h; y++)
		{
			float n2 = Noise::noise3dPerlin(
				x + i, y + j, z + k, seed, randomFloat, randomFloat2 / 10, 128);


			if (n2 > 0.3f && n2 < 0.7f)
			{
				_csystem->addCoords(x + i, (y + j) + 1, z + k, 2);
				_csystem->addCoords(x + i, y + j, z + k, 1);
				
				if ((y + j) - 4 > 0)
				{
					_csystem->addCoords(x + i, (y + j) - 4, z + k, 3);
				}
			}
			/*if (n2 > 0.3f && n2 < 0.5f)
				_csystem->addCoords(x + i, y + j, z + k, 1);*/
		}
	}
}

void Chunk::fillChunk(float i, float j, float k)
{
	_vbo->initialize(32, 32, 32);

	for (float x = 0; x < 32; x++)
	{
		for (float z = 0; z < 32; z++)
		{
			for (int y = 0; y < 32; y++)
			{
				test(x, y, z, i, j, k);
			}
		}
	}
}

void Chunk::rebuildChunk(float i, float j, float k)
{
	for (float x = 0; x < 32; x++)
	{
		for (float z = 0; z < 32; z++)
		{
			for (int y = 0; y < 32; y++)
			{
				if (_csystem->getBlock(x + i, y + j, z + k).x != NULL)
				{
					int sizeX = 1;
					int sizeY = 1;
					int sizeZ = 1;

					if (_csystem->getBlock((x + i) + 1, y + j, z + k).x == NULL)
					{
						_vbo->addVerts(x + i, y + j, z + k, 4, sizeX, sizeY, sizeZ);
						_vbo->addNormals(4);
						_vbo->addUV(4, _bManager->getBlockByID(_csystem->getBlock(x + i, y + j, z + k).x));
					}

					if (_csystem->getBlock((x + i) - 1, y + j, z + k).x == NULL)
					{
						_vbo->addVerts(x + i, y + j, z + k, 3, sizeX, sizeY, sizeZ);
						_vbo->addNormals(3);
						_vbo->addUV(3, _bManager->getBlockByID(_csystem->getBlock(x + i, y + j, z + k).x));
					}

					if (_csystem->getBlock(x + i, (y + j) + 1, z + k).x == NULL)
					{
						_vbo->addVerts(x + i, y + j, z + k, 1, sizeX, sizeY, sizeZ);
						_vbo->addNormals(1);
						_vbo->addUV(1, _bManager->getBlockByID(_csystem->getBlock(x + i, y + j, z + k).x));
					}

					if (_csystem->getBlock(x + i, (y + j) - 1, z + k).x == NULL)
					{
						_vbo->addVerts(x + i, y + j, z + k, 2, sizeX, sizeY, sizeZ);
						_vbo->addNormals(2);
						_vbo->addUV(2, _bManager->getBlockByID(_csystem->getBlock(x + i, y + j, z + k).x));
					}

					if (_csystem->getBlock(x + i, y + j, (z + k) + 1).x == NULL)
					{
						_vbo->addVerts(x + i, y + j, z + k, 5, sizeX, sizeY, sizeZ);
						_vbo->addNormals(5);
						_vbo->addUV(5, _bManager->getBlockByID(_csystem->getBlock(x + i, y + j, z + k).x));
					}

					if (_csystem->getBlock(x + i, y + j, (z + k) - 1).x == NULL)
					{
						_vbo->addVerts(x + i, y + j, z + k, 6, sizeX, sizeY, sizeZ);
						_vbo->addNormals(6);
						_vbo->addUV(6, _bManager->getBlockByID(_csystem->getBlock(x + i, y + j, z + k).x));
					}
				}
			}
		}
	}

	_vbo->buildVBO();
}

void Chunk::buildChunk(float i, float j, float k)
{
	for (float x = 0; x < 32; x++)
	{
		for (float z = 0; z < 32; z++)
		{
			for (int y = 0; y < 32; y++)
			{
				if (_csystem->getBlock(x + i, y + j, z + k).x != NULL)
				{
					int sizeX = 1;
					int sizeY = 1;
					int sizeZ = 1;

					if (_csystem->getBlock((x + i) + 1, y + j, z + k).x == NULL)
					{
						_vbo->addVerts(x + i, y + j, z + k, 4, sizeX, sizeY, sizeZ);
						_vbo->addIndices2();
						_vbo->addNormals(4);
						_vbo->addUV(4, _bManager->getBlockByID(_csystem->getBlock(x + i, y + j, z + k).x));

						voxelCount += 0.16666666666f;
					}

					if ((x + i) - 1 > 0 && _csystem->getBlock((x + i) - 1, y + j, z + k).x == NULL)
					{
						_vbo->addVerts(x + i, y + j, z + k, 3, sizeX, sizeY, sizeZ);
						_vbo->addIndices2();
						_vbo->addNormals(3);
						_vbo->addUV(3, _bManager->getBlockByID(_csystem->getBlock(x + i, y + j, z + k).x));

						voxelCount += 0.16666666666f;
					}

					if (_csystem->getBlock(x + i, (y + j) + 1, z + k).x == NULL)
					{
						_vbo->addVerts(x + i, y + j, z + k, 1, sizeX, sizeY, sizeZ);
						_vbo->addIndices2();
						_vbo->addNormals(1);
						_vbo->addUV(1, _bManager->getBlockByID(_csystem->getBlock(x + i, y + j, z + k).x));

						voxelCount += 0.16666666666f;
					}

					if ((y + j) - 1 > 0 && _csystem->getBlock(x + i, (y + j) - 1, z + k).x == NULL)
					{
						_vbo->addVerts(x + i, y + j, z + k, 2, sizeX, sizeY, sizeZ);
						_vbo->addIndices2();
						_vbo->addNormals(2);
						_vbo->addUV(2, _bManager->getBlockByID(_csystem->getBlock(x + i, y + j, z + k).x));

						voxelCount += 0.16666666666f;
					}

					if (_csystem->getBlock(x + i, y + j, (z + k) + 1).x == NULL)
					{
						_vbo->addVerts(x + i, y + j, z + k, 5, sizeX, sizeY, sizeZ);
						_vbo->addIndices2();
						_vbo->addNormals(5);
						_vbo->addUV(5, _bManager->getBlockByID(_csystem->getBlock(x + i, y + j, z + k).x));

						voxelCount += 0.16666666666f;
					}

					if ((z + k) - 1 > 0 && _csystem->getBlock(x + i, y + j, (z + k) - 1).x == NULL)
					{
						_vbo->addVerts(x + i, y + j, z + k, 6, sizeX, sizeY, sizeZ);
						_vbo->addIndices2();
						_vbo->addNormals(6);
						_vbo->addUV(6, _bManager->getBlockByID(_csystem->getBlock(x + i, y + j, z + k).x));

						voxelCount += 0.16666666666f;
					}
				}
			}
		}
	}

	_vbo->buildVBO();
}

