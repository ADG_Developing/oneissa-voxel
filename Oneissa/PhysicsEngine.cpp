#include "PhysicsEngine.h"

#include "Camera.h"
#include "GlobalStructs.h"
#include "CoordSystem.h"
#include "Chunk.h"
#include "ChunkManager.h"

PhysicsEngine::PhysicsEngine(Camera* cam, ChunkManager* cman, CoordSystem* coordsystem)
{
	_camera = cam;
	_coordSys = coordsystem;
	_cManager = cman;
}

PhysicsEngine::~PhysicsEngine()
{

}

glm::vec3 getChunkByMousePos2(glm::vec3 mousePos)
{
	glm::ivec3 global = (glm::ivec3)mousePos;
	glm::ivec3 chunkSize = glm::ivec3(32, 32, 32);

	//local = glm::ivec3(((global.x % chunkSize.x) + chunkSize.x) % chunkSize.x, ((global.y % chunkSize.y) + chunkSize.y) % chunkSize.y, ((global.x % chunkSize.z) + chunkSize.z) % chunkSize.z);

	float x = ((int)global.x - ((int)global.x % chunkSize.x)) / chunkSize.x;
	float y = ((int)global.y - ((int)global.y % chunkSize.y)) / chunkSize.y;
	float z = ((int)global.z - ((int)global.z % chunkSize.z)) / chunkSize.z;

	cout << global.x << ", " << y << ", " << z << endl;

	return glm::vec3(x, y, z);
}

Chunk* PhysicsEngine::getChunk()
{
	glm::vec3 pos = glm::vec3(_camera->getPosition().x, _camera->getPosition().y, _camera->getPosition().z);
	glm::vec3 currChunkPos = getChunkByMousePos2(pos);

	Chunk* currChunk = (Chunk*)NULL;

	if (_cManager->getChunk(currChunkPos.x * 32, currChunkPos.y * 32, currChunkPos.z * 32) != NULL)
	{
		currChunk = _cManager->getChunk(currChunkPos.x * 32, currChunkPos.y * 32, currChunkPos.z * 32);
	}

	return currChunk;
}

glm::vec3 PhysicsEngine::getRaycastPosition()
{
	glm::vec3 direction = _camera->_direction;
	glm::vec3 startPosition = _camera->_position;
	glm::vec3 currentPosition;

	float maxStep = 100;
	float STEP_SIZE = 0.1f;

	for (float i = 1; i < maxStep; i += STEP_SIZE) {
		currentPosition = startPosition + direction * i;

		if (_coordSys->getBlock(currentPosition.x, currentPosition.y, currentPosition.z).x != NULL)
		{
			break;
		}
	}

	return currentPosition;
}

glm::vec3 PhysicsEngine::getRaycastPositionBlock()
{
	glm::vec3 direction = _camera->_direction;
	glm::vec3 startPosition = _camera->_position;
	glm::vec3 currentPosition;
	glm::vec3 currentPosition2;

	float maxStep = 100;
	float STEP_SIZE = 0.1f;

	for (float i = 1; i < maxStep; i += STEP_SIZE) {
		float i2 = 0;
		i2 = i - STEP_SIZE;

		currentPosition = startPosition + direction * i;

		if (_coordSys->getBlock(currentPosition.x, currentPosition.y, currentPosition.z).x != NULL)
		{
			break;
		}

		currentPosition2 = startPosition + direction * i2;
	}

	return currentPosition2;
}

void PhysicsEngine::updateGravVelocity(int groundHeight)
{
	if (_coordSys->getBlock(_camera->getPosition().x, _camera->getPosition().y - 4, _camera->getPosition().z).x != NULL)
	{
		velocity.y = 0;
	}

	if (Keys[SDLK_SPACE].pr){
		velocity.y = jumpSpeed;
	}

	_camera->setPosition(_camera->_position.x, _camera->_position.y + velocity.y, _camera->_position.z);

	if (_camera->getPosition().y > groundHeight)
	{
		velocity.y -= gravity;
	}
	else
	{
		velocity.y = 0;

		_camera->setPosition(_camera->_position.x, _camera->_position.y - velocity.y, _camera->_position.z);
	}
}