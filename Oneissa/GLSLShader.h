#pragma once

#include <Windows.h>
#include <glew.h>
#include <string>

#define GLSHADER_H

class GLSLShader
{
public:
	GLSLShader();
	~GLSLShader();

	std::string readFile(const char *filePath);
	GLuint LoadShader(const char *vertex_path, const char *fragment_path);
};