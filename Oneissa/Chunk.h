#pragma once

class VBO;
class Camera;
class CoordSystem;
class ChunkManager;
class GLSLShader;
class OneissaMath;
class ContentSystem;
class BlockManager;
class Block;

class Chunk
{
public:
	Chunk(CoordSystem *_coordsys, ChunkManager *_cman, Camera *_camera, OneissaMath *_omath, ContentSystem *_contsystem, BlockManager* _bMan);
	~Chunk();

	VBO *_vbo;

	int cubeCoords[32][32][32];

	float voxelCount = 0;

	void init();
	void fillChunk(float, float, float);
	void buildChunk(float, float, float);
	void rebuildChunk(float i, float j, float k);
	void test(int x, int y, int z, int i, int j, int k);
	//void addBlock(int x, int y, int z, Block* block);
	//Block* getBlock(int x, int y, int z);

private:
	CoordSystem* _csystem;
	Camera* _cam;
	OneissaMath* _oneissaMath;
	ContentSystem* _contentSystem;
	ChunkManager* _cManager;
	BlockManager* _bManager;
};