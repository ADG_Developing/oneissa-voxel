#include "BlockManager.h"

#include "Block.h"

BlockManager::BlockManager()
{

}

BlockManager::~BlockManager()
{

}

void BlockManager::init()
{
	dirt = new Block();
	grass = new Block();
	stone = new Block();
	water = new Block();

	dirt->init(1, 3, 1, 3, 1, 3, 1);
	grass->init(2, 1, 1, 3, 1, 4, 1);
	stone->init(3, 2, 1, 2, 1, 2, 1);
	water->init(4, 14, 13, 14, 13, 14, 13);

	blockList[dirt->blockID] = dirt;
	blockList[grass->blockID] = grass;
	blockList[stone->blockID] = stone;
	blockList[water->blockID] = water;
}

Block* BlockManager::getBlockByID(int id)
{
	if (blockList[id] != NULL)
	{
		return blockList[id];
	}
	else
	{
		return NULL;
	}
}