#pragma once

#include <queue>
#include <glm/glm.hpp>

using namespace std;

class Camera;
class CoordSystem;

class Player
{
public:
	Player(Camera* camera);
	~Player();

	float x;
	float y;
	float z;

	Camera* _camera;

	float getX();
	float getY();
	float getZ();

	glm::vec3 getCurrentChunk();
private:
};