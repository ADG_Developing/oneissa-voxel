#pragma once

#include <queue>
#include <glm/glm.hpp>
#include <iostream>

using namespace std;

class Chunk;
class Player;
class Camera;
class CoordSystem;
class VBO;
class GLSLShader;
class OneissaMath;
class ContentSystem;
class BlockManager;
class Block;

class ChunkManager
{
public:
	ChunkManager(Player* p, CoordSystem *coordsys, Camera *camera, OneissaMath *omath, ContentSystem *contsystem, BlockManager* bMan);
	~ChunkManager();

	int dequeWidth = 128;
	float voxelCount = 0;
	int chunkCount = 0;

	deque<deque<deque<float> > > matrix;

	void init();
	void buildWorld();
	void loadChunk(float, float, float);
	void unloadChunk(float, float, float);
	void buildChunk(float, float, float);
	Chunk* getChunk(float, float, float);
	void addChunk(float, float, float);
	void renderWorld();

private:
	Chunk *chunkList[255][255][255];
	Player *_player;
	CoordSystem *_coordsystem;
	Camera *_cam;
	GLSLShader *sManager;
	OneissaMath *_oneissaMath;
	ContentSystem *_contentSystem;
	BlockManager* _bManager;

	int cubeShader = 0;
};