#pragma once

#include <glew.h>

#include <iostream>
#include <new>
#include <vector>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/noise.hpp>
#include <glm/gtx/random.hpp> // vecRand3 
#include <glm/gtc/matrix_transform.hpp>

#include <vector>
#include <queue>
#include <iterator>

using namespace std;

class Camera;
class OneissaMath;
class ContentSystem;
class BlockManager;
class Block;

//Class that handles the graphics and rendering for voxels
class VBO
{
public:
	VBO(OneissaMath* _omath, ContentSystem* _contsystem, BlockManager* _bMan);
	~VBO();

	void buildVBO();
	void shader(GLuint programID, GLuint cubeShader2, Camera *gcamera);
	void initialize(int w, int h, int l);
	void addIndices(int w, int h, int l);
	void addIndices2();
	void addNormals(int facing);
	void addUV(int facing, Block* block);
	void addVerts(float x, float y, float z, int facing, float w, float h, float l);
	void renderVoxels();

	std::vector<glm::vec3> cubeVerts;
	std::vector<glm::vec3> cubeNormals;
	std::vector<glm::vec2> cubeUVs;
	std::vector<unsigned int> cubeIndices;

	glm::mat4 Projection;

private:
	std::vector<glm::vec3> verticesFrontBuf;  // Temp holding buffer.
	std::vector<glm::vec3> verticesBackBuf;   // Temp holding buffer.
	std::vector<glm::vec3> verticesLeftBuf;   // Temp holding buffer.
	std::vector<glm::vec3> verticesRightBuf;  // Temp holding buffer.
	std::vector<glm::vec3> verticesAboveBuf;  // Temp holding buffer.
	std::vector<glm::vec3> verticesBelowBuf;  // Temp holding buffer.

	int indiceCounter = 0;
	int indiceCounter2 = 0;

	GLuint vertexBuffer;
	GLuint uvBuffer;
	GLuint normalBuffer;
	GLuint indexBuffer;
	//GLuint *cubeIndices;

	OneissaMath *_oneissaMath;
	ContentSystem *_contentSystem;

	int texture[100];
};

