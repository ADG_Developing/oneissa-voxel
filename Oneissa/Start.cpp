#pragma once
#define _CRT_SECURE_NO_WARNINGS

#ifdef _WIN32
#include <windows.h>
#endif

#include <glew.h>

#include <SDL.h>
#include <SDL_ttf.h>
#include <iostream>
#include <thread>
#include "Camera.h"
#include "GlobalStructs.h"
#include "GameManager.h"

#include <stdlib.h>
#include <time.h> 
#include <string>

using namespace std;

void initialize();
void initializeSdlOpengl();
void initializeShaders();
void control();
void update();
void draw();
void destroy();
bool checkGlError();

enum states{ INITIALIZE, PLAY, EXIT };

int gameState;

//  The number of frames
int frameCount = 0;
int frameCount2 = 0;

//  Number of frames per second
float fps = 0;
float fps2 = 0;

//  currentTime - previousTime is the time elapsed
//  between every call of the Idle function
int currentTime = 0, previousTime = 0;
int currentTime2 = 0, previousTime2 = 0;

bool g_fps_mode = false;

SDL_Window* mainWindow;
SDL_Renderer* renderer;
SDL_GLContext mainOpenGLContext;

Camera *mainCamera;

GameManager* gmanager;

void calculateFPS()
{
	//  Increase frame count
	frameCount++;

	//  Get the number of milliseconds since glutInit called 
	//  (or first call to glutGet(GLUT ELAPSED TIME)).
	currentTime = GetTickCount();

	//  Calculate time passed
	int timeInterval = currentTime - previousTime;

	if (timeInterval > 1000)
	{
		//  calculate the number of frames per second
		fps = frameCount / (timeInterval / 1000.0f);

		//  Set time
		previousTime = currentTime;

		//  Reset frame count
		frameCount = 0;
	}
}

void calculateFPS2()
{
	//  Increase frame count
	frameCount2++;

	//  Get the number of milliseconds since glutInit called 
	//  (or first call to glutGet(GLUT ELAPSED TIME)).
	currentTime2 = GetTickCount();

	//  Calculate time passed
	int timeInterval = currentTime2 - previousTime2;

	if (timeInterval > 1000)
	{
		//  calculate the number of frames per second
		fps2 = frameCount2 / (timeInterval / 1000.0f);

		//  Set time
		previousTime2 = currentTime2;

		//  Reset frame count
		frameCount2 = 0;
	}
}

int main(int argc, char **argv)
{
	srand(time(NULL));
	initialize();

	while (gameState == PLAY){
		control();
		update();
		draw();
	}

	destroy();
	return 0;
}

void initialize()
{
	gameState = INITIALIZE;

	initializeSdlOpengl();

	mainCamera = new Camera();

	gmanager = new GameManager(mainCamera);
	gmanager->init(renderer);
	gmanager->build();

	gameState = PLAY;
}

void initializeSdlOpengl()
{
	SDL_Init(SDL_INIT_EVERYTHING);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	cout << "Video Driver: " << SDL_GetCurrentVideoDriver() << endl;
	SDL_DisplayMode mode;
	SDL_GetCurrentDisplayMode(0, &mode);
	cout << "Window Info - W: " << mode.w << " H: " << mode.h << " Refresh: " << mode.refresh_rate << " " << endl;

	Uint32 wflags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;
	//if (graphicsOptions.isFullscreen) wflags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
	//if (graphicsOptions.isBorderless) wflags |= SDL_WINDOW_BORDERLESS;
	mainWindow = SDL_CreateWindow("Oneissa", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 800, wflags);
	renderer = SDL_CreateRenderer(mainWindow, -1, 0);

	if (mainWindow == NULL){
		exit(343);
	}

	mainOpenGLContext = SDL_GL_CreateContext(mainWindow);
	if (mainOpenGLContext == NULL){
		//error("Could not make openGL context!");
		SDL_Quit();
		TTF_Quit();
	}
	SDL_GL_MakeCurrent(mainWindow, mainOpenGLContext);

	SDL_GL_SetSwapInterval((int)true);

	GLenum err = glewInit();
	if (err != GLEW_OK){
		int in;
		//error("Glew failed to initialize. Your graphics card is probably WAY too old. Or you forgot to extract the .zip. It might be time for an upgrade :)");
		exit(133);
	}

	int vminor;
	int vmajor;
	glGetIntegerv(GL_MAJOR_VERSION, &vmajor);
	glGetIntegerv(GL_MINOR_VERSION, &vminor);
	printf("\n***		Opengl Version: %s\n", glGetString(GL_VERSION));
	printf("\n***       CPU Threads: %u\n", thread::hardware_concurrency());

	//check that opengl versio is at least 3
	if (vmajor < 3){// I dont think its works like I think it does
		char buffer[2048];
		sprintf(buffer, "Your graphics card driver does not support at least OpenGL 3.3. Your OpenGL version is \"%s\". The game will most likely not work.\n\nEither your graphics card drivers are not up to date, or your computer is using an integrated graphics card instead of your gaming card.\nYou should be able to switch to your main graphics card by right clicking your desktop and going to graphics properties.", glGetString(GL_VERSION));
		//error(buffer);
		//		exit(133);
	}

	if (!GLEW_VERSION_2_1){  // check that the machine supports the 2.1 API.
		int in;
		//error("Machine does not support 2.1 GLEW API.");
		exit(134);
	}

	int textureunits;
	glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &textureunits);
	if (textureunits < 8){
		//error("Your graphics card does not support at least 8 texture units! It only supports " + to_string(textureunits));
		exit(1);
	}

	glClearColor(135.0 / 255.0, 206.0 / 255.0, 250.0 / 255.0, 0.0); // Skyblue background
	//glLineWidth(1);

	//Alpha
	glAlphaFunc(GL_GREATER, 0.0f);
	glEnable(GL_ALPHA_TEST);

	glDisable(GL_CULL_FACE);
	glDisable(GL_MULTISAMPLE);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);

	glDepthRange(0.0f, 1.0f);

	if (TTF_Init() == -1){
		printf("Unable to initialize SDL_ttf: %s \n", TTF_GetError());
		SDL_Quit();
		TTF_Quit();
		TTF_CloseFont(gmanager->font);
	}
}

void control() {
	SDL_Event evnt;

	while (SDL_PollEvent(&evnt)) {
		switch (evnt.type) {
		case SDL_QUIT:
			gameState = EXIT;
			return;
		case SDL_MOUSEMOTION:
			if (g_fps_mode) {
				mainCamera->mouseMove(evnt.motion.xrel, evnt.motion.yrel);
			}
			break;
		case SDL_MOUSEBUTTONDOWN:
			if (evnt.button.button == SDL_BUTTON_LEFT) {
				gmanager->removeBlock();
			}

			if (evnt.button.button == SDL_BUTTON_RIGHT) {
				gmanager->addBlock(3, 1);
			}

			MouseButtons[evnt.button.button] = true;
			break;
		case SDL_MOUSEBUTTONUP:
			std::cout << "Mouse button released!" << std::endl;
			MouseButtons[evnt.button.button] = false;
			break;
		case SDL_MOUSEWHEEL:
			mainCamera->mouseZoom(evnt.wheel.y);
			break;
		case SDL_KEYDOWN:
			Keys[evnt.key.keysym.sym].pr = true;
			switch (evnt.key.keysym.sym){
			    case SDLK_r:
					if (Keys[SDLK_r].pr == 1){
						g_fps_mode = !g_fps_mode;

						if (g_fps_mode) {
							SDL_ShowCursor(0);
							SDL_SetRelativeMouseMode((SDL_bool)true);
						}
						else {
							SDL_ShowCursor(1);
							SDL_SetRelativeMouseMode((SDL_bool)false);
						}
					}
				break;

				case SDLK_ESCAPE:
					if (Keys[SDLK_ESCAPE].pr == 1){
						exit(0);
						destroy();
					}
				break;
			}
			break;

		case SDL_KEYUP:
			Keys[evnt.key.keysym.sym].pr = false;
			break;
		}
	}
}

void update() {
	calculateFPS2();
	mainCamera->update();
}

void draw() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearDepth(1.0);

	gmanager->loop(renderer);

	string s = to_string(floor(fps));
	string s2 = to_string(floor(fps2));
	string s3 = "Graphics FPS: " + s + ", Physics FPS: " + s2;

	SDL_SetWindowTitle(mainWindow, s3.c_str());

	calculateFPS();
	SDL_GL_SwapWindow(mainWindow);
}

void destroy() {
	SDL_GL_DeleteContext(mainOpenGLContext);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(mainWindow);
	SDL_Quit();
	TTF_Quit();
	TTF_CloseFont(gmanager->font);
}