#include "OneissaMath.h"

OneissaMath::OneissaMath()
{

}

OneissaMath::~OneissaMath()
{

}

glm::vec2 OneissaMath::calculateUV(int x, int y, float sizeX, float sizeY)
{
	float outx = 0;
	float outy = 0;

	outx = ((x * 16.00f) / sizeX);
	outy = 1.00f - ((y * 16.00f) / sizeY);

	return glm::vec2(outx, outy);
}