#pragma once
class Planet
{
public:
	Planet();
	~Planet();

	// Composition vars
	float co = 0;
	float co2 = 0;
	float iron = 0;
	float stone = 0;
	float water = 0;
	float oxygen = 0;
	float nitrogen = 0;
	float hydrogen = 0;
	float silicate = 0;
	float organics = 0;

	// Noise vars
	float baseNoiseOctaves = 0;
	float baseNoisePersistance = 0;
	float baseNoiseSeed = 0;
};

