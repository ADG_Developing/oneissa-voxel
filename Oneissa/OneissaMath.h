#pragma once

#include <glm/glm.hpp>

class OneissaMath
{
public:
	OneissaMath();
	~OneissaMath();

	glm::vec2 calculateUV(int x, int y, float sizeX, float sizeY);
};