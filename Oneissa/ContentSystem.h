#pragma once

#include <SOIL.h>
#include <glew.h>
#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>
#include <iostream>

class ContentSystem
{
public:
	ContentSystem();
	~ContentSystem();

	GLuint texture[100];

	GLuint loadPNG(int id, char* path);
	TTF_Font* loadfont(char* file, int ptsize);
};