#pragma once

#include <glew.h>

#include <iostream>
#include <new>
#include <vector>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/noise.hpp>
#include <glm/gtx/random.hpp> // vecRand3 
#include <glm/gtc/matrix_transform.hpp>

#include <vector>
#include <queue>
#include <iterator>

using namespace std;

class CoordSystem;
class ContentSystem;
class OneissaMath;
class Camera;

class Mesher
{
public:
	Mesher();
	~Mesher();

	ContentSystem* _contentSystem;
	CoordSystem* _coordSystem;
	OneissaMath* _oneissaMath;

	//std::vector<glm::vec3> vboVerts;
	std::vector<glm::vec3> cubeVerts;
	std::vector<glm::vec2> cubeUV;
	std::vector<glm::vec3> normals;
	std::vector<unsigned short> indices;
	//std::vector<glm::vec3> cubeVertsBuf;

	std::vector<glm::vec3> verticesFrontBuf;  // Temp holding buffer.
	std::vector<glm::vec3> verticesBackBuf;   // Temp holding buffer.
	std::vector<glm::vec3> verticesLeftBuf;   // Temp holding buffer.
	std::vector<glm::vec3> verticesRightBuf;  // Temp holding buffer.
	std::vector<glm::vec3> verticesAboveBuf;  // Temp holding buffer.
	std::vector<glm::vec3> verticesBelowBuf;  // Temp holding buffer.

	GLuint cubeVertsVBO;
	GLuint uvbuffer;
	GLuint normalID;
	GLuint elementbuffer;

	enum types{ AIR = NULL };

	// OpenGL Attributes
	GLint posAttrib;
	GLuint normAttrib;
	GLuint worldPosAttrib;

	glm::vec3 c_pos;

	float frame = 0;

	int voxelCount = 0;
	int texture[5];

	void init(OneissaMath* om, CoordSystem* cs, ContentSystem* contentsys);
	void draw();
	void addUV();
	void buildVBO();
	void addNormals();
	void addIndices();
	void addFace(float x, float y, float z, int facing, int sizeX, int sizeY, int sizeZ);
	void shader(GLuint programID, GLuint cubeShader2, Camera *gcamera);
	void addChunk(int x2, int y2, int z2);
	void fillChunk(int x, int y, int z);
	void createTerrain2();
};