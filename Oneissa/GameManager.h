#pragma once

#include <queue>
#include <glew.h>
#include <SDL.h>
#include <SDL_ttf.h>
#include <glm/glm.hpp>

using namespace std;

class ContentSystem;
class ChunkManager;
class CoordSystem;
class OneissaMath;
class GLSLShader;
class Camera;
class Mesher;
class Player;
class Chunk;
class VBO;
class PhysicsEngine;
class TextManager;
class BlockManager;
class Block;

class GameManager
{
public:
	GameManager(Camera *c);
	~GameManager();

	void init(SDL_Renderer* renderer);
	void loop(SDL_Renderer* renderer);
	void render();
	void build();

	void addBlock(int type, float height);
	void addBlock2(int x, int y, int z, int type, float height);
	void removeBlock();
	void removeBlock2(int x, int y, int z);
	int getBlock(glm::vec3 pos);
	void setBlock(glm::vec3 pos, int type);

	GLSLShader* shaderManager;
	CoordSystem *coordSystem;
	ChunkManager *cManager;
	ContentSystem *cSystem;
	OneissaMath *oMath;
	Camera *_camera;
	Player *player;
	PhysicsEngine* physicsEngine;
	TextManager* textManager;
	BlockManager* bManager;

	TTF_Font *font;
	
private:
};