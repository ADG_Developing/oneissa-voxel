#include "Player.h"

#include "Camera.h"

#include <iostream>

Player::Player(Camera* camera)
{
	_camera = camera;
}

Player::~Player()
{

}

float Player::getX()
{
	glm::vec3 pos = _camera->getPosition();

	return pos.x;
}

float Player::getY()
{
	glm::vec3 pos = _camera->getPosition();

	return pos.y;
}

float Player::getZ()
{
	glm::vec3 pos = _camera->getPosition();

	return pos.z;
}

glm::vec3 Player::getCurrentChunk()
{
	glm::ivec3 global = glm::ivec3(getX(), getY(), getZ());
	glm::ivec3 chunkSize = glm::ivec3(16, 16, 16);

	//local = glm::ivec3(((global.x % chunkSize.x) + chunkSize.x) % chunkSize.x, ((global.y % chunkSize.y) + chunkSize.y) % chunkSize.y, ((global.x % chunkSize.z) + chunkSize.z) % chunkSize.z);

	float x = ((int)getX() - ((int)getX() % chunkSize.x)) / chunkSize.x;
	float y = ((int)getY() - ((int)getY() % chunkSize.y)) / chunkSize.y;
	float z = ((int)getZ() - ((int)getZ() % chunkSize.z)) / chunkSize.z;

	cout << x * 16 << ", " << y * 16 << ", " << z * 16 << endl;

	return glm::vec3(x, y, z);
}
