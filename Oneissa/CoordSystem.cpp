#include "CoordSystem.h"

#include "DPackage.h"
#include "Block.h"
#include "Chunk.h";
#include "ChunkManager.h"

glm::vec2 cubeCoords[512][512][512];

int bgIDCounter = 0;

CoordSystem::CoordSystem()
{

}

CoordSystem::~CoordSystem()
{

}

void CoordSystem::removeCoords(int x, int y, int z)
{
	cubeCoords[x][y][z] = (glm::vec2)NULL;
}

void CoordSystem::addCoords(int x, int y, int z, int type)
{
	cubeCoords[x][y][z] = glm::vec2(type, bgIDCounter);
	
	bgIDCounter += 1;
}

void CoordSystem::moveBlock(int x, int y, int z, int toX, int toY, int toZ, int type)
{
	cubeCoords[toX][toY][toZ] = glm::vec2(type, cubeCoords[x][y][z].y);
	cubeCoords[x][y][z] = (glm::vec2)NULL;
}

glm::vec2 CoordSystem::getBlock(int x, int y, int z)
{
	return cubeCoords[x][y][z];

	//if (x == 0 && y == 0 && z == 0 || x > 0 && y > 0 && z >> 0)
	//{
	//	return cubeCoords[x][y][z];
	//}
	//else
	//{
	//	return 0;
	//}
}