#pragma once

#include <glm\glm.hpp>

using namespace std;

class Block;
class Chunk;
class ChunkManager;

class CoordSystem
{
public:
	CoordSystem();
	~CoordSystem();

	void addCoords(int x, int y, int z, int type);
	void removeCoords(int x, int y, int z);
	glm::vec2 getBlock(int x, int y, int z);
	void moveBlock(int x, int y, int z, int toX, int toY, int toZ, int type);
};