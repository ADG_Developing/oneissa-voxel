#include "DPackage.h"

DPackage::DPackage(float varf1, float varf2, float varf3, char* varc1, char* varc2, char* varc3, std::string varstr1, std::string varstr2, std::string varstr3)
{
	float1 = varf1;
	float2 = varf2;
	float3 = varf3;
	str1 = varstr1;
	str2 = varstr2;
	str3 = varstr3;
	char1 = varc1;
	char2 = varc2;
	char3 = varc3;
}

DPackage::DPackage(float varf1, float varf2, float varf3)
{
	float1 = varf1;
	float2 = varf2;
	float3 = varf3;
}

DPackage::DPackage(float varf1, float varf2, float varf3, int blocktype, int bgID)
{
	float1 = varf1;
	float2 = varf2;
	float3 = varf3;
	blocktype1 = blocktype;
	blockgridID = bgID;
}

DPackage::DPackage(char* varc1, char* varc2, char* varc3)
{
	char1 = varc1;
	char2 = varc2;
	char3 = varc3;
}

DPackage::DPackage(std::string varstr1, std::string varstr2, std::string varstr3)
{
	str1 = varstr1;
	str2 = varstr2;
	str3 = varstr3;
}