//#include "Mesher.h"
//
//#include "CoordSystem.h"
//#include "ContentSystem.h"
//#include "OneissaMath.h"
//#include "Camera.h"
//
//Mesher::Mesher()
//{
//
//}
//
//Mesher::~Mesher()
//{
//
//}
//
//void Mesher::init(OneissaMath* om, CoordSystem* cs, ContentSystem* contentsys)
//{
//	_oneissaMath = om;
//	_coordSystem = cs;
//	_contentSystem = contentsys;
//
//	//texture[0] = contentSystem->loadPNG(1, "../data/textures/blocks/gneiss_top.png");
//	//texture[1] = contentSystem->loadPNG(1, "../data/textures/blocks/stone.png");
//	//texture[2] = contentSystem->loadPNG(1, "../data/textures/blocks/gneiss_side.png");
//	//texture[3] = contentSystem->loadPNG(1, "../data/textures/blocks/dolomite.png");
//	//texture[4] = contentSystem->loadPNG(1, "../data/textures/blocks/granite.png");
//	texture[0] = _contentSystem->loadPNG(1, "../data/textures/blocks/uvtemplate.png");
//
//	glGenBuffers(1, &cubeVertsVBO);
//	glGenBuffers(1, &uvbuffer);
//	glGenBuffers(1, &normalID);
//	glGenBuffers(1, &elementbuffer);
//
//	glEnable(GL_TEXTURE_2D);
//
//	srand(time(0));
//}
//
//void Mesher::draw()
//{
//	//glPolygonMode(GL_FRONT, GL_LINE);
//	//glPolygonMode(GL_BACK, GL_LINE);
//
//	if (!cubeVerts.empty())
//	{
//		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
//		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
//		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//
//		glBindTexture(GL_TEXTURE_2D, texture[(int)frame]);
//
//		// 3rd attribute buffer : normals
//		glEnableVertexAttribArray(2);
//		glBindBuffer(GL_ARRAY_BUFFER, normalID);
//		glVertexAttribPointer(
//			2,                                // attribute
//			3,                                // size
//			GL_FLOAT,                         // type
//			GL_FALSE,                         // normalized?
//			0,                                // stride
//			(void*)0                          // array buffer offset
//			);
//
//		// 2nd attribute buffer : UVs
//		glEnableVertexAttribArray(1);
//		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
//		glVertexAttribPointer(
//			1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
//			2,                                // size : U+V => 2
//			GL_FLOAT,                         // type
//			GL_FALSE,                         // normalized?
//			0,                                // stride
//			(void*)0                          // array buffer offset
//			);
//
//		glEnableVertexAttribArray(0);
//		glBindBuffer(GL_ARRAY_BUFFER, cubeVertsVBO);
//		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
//
//		// Index buffer
//		//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
//
//		// Draw the triangles !
//		glDrawElements(
//			GL_TRIANGLES,      // mode
//			indices.size() / 3,    // count
//			GL_UNSIGNED_SHORT,   // type
//			(void*)0           // element array buffer offset
//			);
//
//		glDrawArrays(GL_TRIANGLES, 0, cubeVerts.size());
//
//		glBindBuffer(GL_ARRAY_BUFFER, 0);
//		glBindTexture(GL_TEXTURE_2D, 0);
//
//		glDisableVertexAttribArray(0);
//		glDisableVertexAttribArray(1);
//		glDisableVertexAttribArray(2);
//	}
//
//	frame += 0.002;
//
//	if (frame > 1)
//	{
//		frame = 0;
//	}
//}
//
//void Mesher::addUV()
//{
//	glm::vec2 v;
//
//	v.x = _oneissaMath->calculateUV(1, 1, 6, 6).x; v.y = _oneissaMath->calculateUV(1, 1, 6, 6).y; cubeUV.push_back(v);
//	v.x = _oneissaMath->calculateUV(1, 0, 6, 6).x; v.y = _oneissaMath->calculateUV(1, 0, 6, 6).y; cubeUV.push_back(v);
//	v.x = _oneissaMath->calculateUV(0, 1, 6, 6).x; v.y = _oneissaMath->calculateUV(0, 1, 6, 6).y; cubeUV.push_back(v);
//	v.x = _oneissaMath->calculateUV(0, 0, 6, 6).x; v.y = _oneissaMath->calculateUV(0, 0, 6, 6).y; cubeUV.push_back(v);
//
//	v.x = _oneissaMath->calculateUV(2, 2, 6, 6).x; v.y = _oneissaMath->calculateUV(1, 1, 6, 6).y; cubeUV.push_back(v);
//	v.x = _oneissaMath->calculateUV(2, 0, 6, 6).x; v.y = _oneissaMath->calculateUV(1, 0, 6, 6).y; cubeUV.push_back(v);
//	v.x = _oneissaMath->calculateUV(0, 2, 6, 6).x; v.y = _oneissaMath->calculateUV(0, 1, 6, 6).y; cubeUV.push_back(v);
//	v.x = _oneissaMath->calculateUV(0, 0, 6, 6).x; v.y = _oneissaMath->calculateUV(0, 0, 6, 6).y; cubeUV.push_back(v);
//
//	v.x = _oneissaMath->calculateUV(3, 3, 6, 6).x; v.y = _oneissaMath->calculateUV(1, 1, 6, 6).y; cubeUV.push_back(v);
//	v.x = _oneissaMath->calculateUV(3, 0, 6, 6).x; v.y = _oneissaMath->calculateUV(1, 0, 6, 6).y; cubeUV.push_back(v);
//	v.x = _oneissaMath->calculateUV(0, 3, 6, 6).x; v.y = _oneissaMath->calculateUV(0, 1, 6, 6).y; cubeUV.push_back(v);
//	v.x = _oneissaMath->calculateUV(0, 0, 6, 6).x; v.y = _oneissaMath->calculateUV(0, 0, 6, 6).y; cubeUV.push_back(v);
//
//	v.x = _oneissaMath->calculateUV(1, 1, 6, 6).x; v.y = _oneissaMath->calculateUV(2, 2, 6, 6).y; cubeUV.push_back(v);
//	v.x = _oneissaMath->calculateUV(1, 0, 6, 6).x; v.y = _oneissaMath->calculateUV(2, 0, 6, 6).y; cubeUV.push_back(v);
//	v.x = _oneissaMath->calculateUV(0, 1, 6, 6).x; v.y = _oneissaMath->calculateUV(0, 2, 6, 6).y; cubeUV.push_back(v);
//	v.x = _oneissaMath->calculateUV(0, 0, 6, 6).x; v.y = _oneissaMath->calculateUV(0, 0, 6, 6).y; cubeUV.push_back(v);
//
//	v.x = _oneissaMath->calculateUV(2, 2, 6, 6).x; v.y = _oneissaMath->calculateUV(2, 2, 6, 6).y; cubeUV.push_back(v);
//	v.x = _oneissaMath->calculateUV(2, 0, 6, 6).x; v.y = _oneissaMath->calculateUV(2, 0, 6, 6).y; cubeUV.push_back(v);
//	v.x = _oneissaMath->calculateUV(0, 2, 6, 6).x; v.y = _oneissaMath->calculateUV(0, 2, 6, 6).y; cubeUV.push_back(v);
//	v.x = _oneissaMath->calculateUV(0, 0, 6, 6).x; v.y = _oneissaMath->calculateUV(0, 0, 6, 6).y; cubeUV.push_back(v);
//
//	v.x = _oneissaMath->calculateUV(3, 3, 6, 6).x; v.y = _oneissaMath->calculateUV(2, 2, 6, 6).y; cubeUV.push_back(v);
//	v.x = _oneissaMath->calculateUV(3, 0, 6, 6).x; v.y = _oneissaMath->calculateUV(2, 0, 6, 6).y; cubeUV.push_back(v);
//	v.x = _oneissaMath->calculateUV(0, 3, 6, 6).x; v.y = _oneissaMath->calculateUV(0, 2, 6, 6).y; cubeUV.push_back(v);
//	v.x = _oneissaMath->calculateUV(0, 0, 6, 6).x; v.y = _oneissaMath->calculateUV(0, 0, 6, 6).y; cubeUV.push_back(v);
//
//	//v.x = 1; v.y = 1; cubeUV.push_back(v);
//	//v.x = 1; v.y = 0; cubeUV.push_back(v);
//	//v.x = 0; v.y = 1; cubeUV.push_back(v);
//	//v.x = 0; v.y = 0; cubeUV.push_back(v);
//
//	//v.x = 1; v.y = 1; cubeUV.push_back(v);
//	//v.x = 1; v.y = 0; cubeUV.push_back(v);
//	//v.x = 0; v.y = 1; cubeUV.push_back(v);
//	//v.x = 0; v.y = 0; cubeUV.push_back(v);
//
//	//v.x = 1; v.y = 1; cubeUV.push_back(v);
//	//v.x = 1; v.y = 0; cubeUV.push_back(v);
//	//v.x = 0; v.y = 1; cubeUV.push_back(v);
//	//v.x = 0; v.y = 0; cubeUV.push_back(v);
//
//	//v.x = 1; v.y = 1; cubeUV.push_back(v);
//	//v.x = 1; v.y = 0; cubeUV.push_back(v);
//	//v.x = 0; v.y = 1; cubeUV.push_back(v);
//	//v.x = 0; v.y = 0; cubeUV.push_back(v);
//
//	//v.x = 1; v.y = 1; cubeUV.push_back(v);
//	//v.x = 1; v.y = 0; cubeUV.push_back(v);
//	//v.x = 0; v.y = 1; cubeUV.push_back(v);
//	//v.x = 0; v.y = 0; cubeUV.push_back(v);
//
//	//v.x = 1; v.y = 1; cubeUV.push_back(v);
//	//v.x = 1; v.y = 0; cubeUV.push_back(v);
//	//v.x = 0; v.y = 1; cubeUV.push_back(v);
//	//v.x = 0; v.y = 0; cubeUV.push_back(v);
//}
//
//void Mesher::buildVBO()
//{
//	int vertex = 1;
//
//	if (!cubeVerts.empty())
//	{
//		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
//		glBufferData(GL_ARRAY_BUFFER, cubeUV.size() * sizeof(glm::vec2), &cubeUV[0], GL_STATIC_DRAW);
//
//		glBindBuffer(GL_ARRAY_BUFFER, cubeVertsVBO);
//		glBufferData(GL_ARRAY_BUFFER, cubeVerts.size() * sizeof(glm::vec3), &cubeVerts[0], GL_STATIC_DRAW);
//
//		glBindBuffer(GL_ARRAY_BUFFER, normalID);
//		glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);
//
//		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
//		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);
//	}
//	glBindBuffer(GL_ARRAY_BUFFER, 0);
//}
//
//void Mesher::addNormals()
//{
//	glm::vec3 v2;
//
//	// Back
//	v2.x = 0; v2.y = 0; v2.z = -1; normals.push_back(v2);
//	v2.x = 0; v2.y = 0; v2.z = -1; normals.push_back(v2);
//	v2.x = 0; v2.y = 0; v2.z = -1; normals.push_back(v2);
//	v2.x = 0; v2.y = 0; v2.z = -1; normals.push_back(v2);
//	v2.x = 0; v2.y = 0; v2.z = -1; normals.push_back(v2);
//	v2.x = 0; v2.y = 0; v2.z = -1; normals.push_back(v2);
//
//	// Front
//	v2.x = 0; v2.y = 0; v2.z = 1; normals.push_back(v2);
//	v2.x = 0; v2.y = 0; v2.z = 1; normals.push_back(v2);
//	v2.x = 0; v2.y = 0; v2.z = 1; normals.push_back(v2);
//	v2.x = 0; v2.y = 0; v2.z = 1; normals.push_back(v2);
//	v2.x = 0; v2.y = 0; v2.z = 1; normals.push_back(v2);
//	v2.x = 0; v2.y = 0; v2.z = 1; normals.push_back(v2);
//
//	// Left
//	v2.x = -1; v2.y = 0; v2.z = 0; normals.push_back(v2);
//	v2.x = -1; v2.y = 0; v2.z = 0; normals.push_back(v2);
//	v2.x = -1; v2.y = 0; v2.z = 0; normals.push_back(v2);
//	v2.x = -1; v2.y = 0; v2.z = 0; normals.push_back(v2);
//	v2.x = -1; v2.y = 0; v2.z = 0; normals.push_back(v2);
//	v2.x = -1; v2.y = 0; v2.z = 0; normals.push_back(v2);
//
//	// Right
//	v2.x = 1; v2.y = 0; v2.z = 0; normals.push_back(v2);
//	v2.x = 1; v2.y = 0; v2.z = 0; normals.push_back(v2);
//	v2.x = 1; v2.y = 0; v2.z = 0; normals.push_back(v2);
//	v2.x = 1; v2.y = 0; v2.z = 0; normals.push_back(v2);
//	v2.x = 1; v2.y = 0; v2.z = 0; normals.push_back(v2);
//	v2.x = 1; v2.y = 0; v2.z = 0; normals.push_back(v2);
//
//	// Bottom
//	v2.x = 0; v2.y = -1; v2.z = 0; normals.push_back(v2);
//	v2.x = 0; v2.y = -1; v2.z = 0; normals.push_back(v2);
//	v2.x = 0; v2.y = -1; v2.z = 0; normals.push_back(v2);
//	v2.x = 0; v2.y = -1; v2.z = 0; normals.push_back(v2);
//	v2.x = 0; v2.y = -1; v2.z = 0; normals.push_back(v2);
//	v2.x = 0; v2.y = -1; v2.z = 0; normals.push_back(v2);
//
//	// Top
//	v2.x = 0; v2.y = 1; v2.z = 0; normals.push_back(v2);
//	v2.x = 0; v2.y = 1; v2.z = 0; normals.push_back(v2);
//	v2.x = 0; v2.y = 1; v2.z = 0; normals.push_back(v2);
//	v2.x = 0; v2.y = 1; v2.z = 0; normals.push_back(v2);
//	v2.x = 0; v2.y = 1; v2.z = 0; normals.push_back(v2);
//	v2.x = 0; v2.y = 1; v2.z = 0; normals.push_back(v2);
//}
//
//void Mesher::addIndices()
//{
//	//indices.push_back(0);
//	//indices.push_back(1);
//	//indices.push_back(2);
//	//indices.push_back(0);
//	//indices.push_back(2);
//	//indices.push_back(3);
//	//indices.push_back(0);
//	//indices.push_back(4);
//	//indices.push_back(5);
//	//indices.push_back(0);
//	//indices.push_back(5);
//	//indices.push_back(1);
//
//	indices.push_back(0);
//	indices.push_back(1);
//	indices.push_back(2);
//
//	indices.push_back(0);
//	indices.push_back(1);
//	indices.push_back(2);
//
//	indices.push_back(0);
//	indices.push_back(1);
//	indices.push_back(2);
//
//	indices.push_back(0);
//	indices.push_back(1);
//	indices.push_back(2);
//
//	indices.push_back(0);
//	indices.push_back(1);
//	indices.push_back(2);
//
//	indices.push_back(0);
//	indices.push_back(1);
//	indices.push_back(2);
//
//	indices.push_back(0);
//	indices.push_back(1);
//	indices.push_back(2);
//
//	indices.push_back(0);
//	indices.push_back(1);
//	indices.push_back(2);
//
//	indices.push_back(0);
//	indices.push_back(1);
//	indices.push_back(2);
//
//	indices.push_back(0);
//	indices.push_back(1);
//	indices.push_back(2);
//
//	indices.push_back(0);
//	indices.push_back(1);
//	indices.push_back(2);
//
//	indices.push_back(0);
//	indices.push_back(1);
//	indices.push_back(2);
//}
//
//void Mesher::addFace(float x, float y, float z, int facing, int sizeX, int sizeY, int sizeZ)
//{
//	glm::vec3 v;
//	int sX = sizeX;
//	int sY = sizeY;
//	int sZ = sizeZ;
//
//	//v.w = ; // Vector.w is static.
//
//	enum f{ TOP = 1, BOTTOM = 2, LEFT = 3, RIGHT = 4, FRONT = 5, BACK = 6 };
//
//	switch (facing)
//	{
//	case TOP:
//		// Add a new face.
//		v.x = x + sizeX; v.y = y + sizeY; v.z = z + sizeZ; cubeVerts.push_back(v); //C 
//		v.x = x + sizeX; v.y = y + sizeY; v.z = z;   cubeVerts.push_back(v); //D
//		v.x = x;   v.y = y + sizeY; v.z = z + sizeZ; cubeVerts.push_back(v); //B
//		v.x = x;   v.y = y + sizeY; v.z = z + sizeZ; cubeVerts.push_back(v); //B 
//		v.x = x + sizeX; v.y = y + sizeY; v.z = z;   cubeVerts.push_back(v); //D
//		v.x = x;   v.y = y + sizeY; v.z = z;   cubeVerts.push_back(v); //A
//		break;
//	case BOTTOM:
//		// Add a new face.
//		v.x = x + sizeX; v.y = y;   v.z = z + sizeZ; cubeVerts.push_back(v); //C
//		v.x = x;   v.y = y;   v.z = z + sizeZ; cubeVerts.push_back(v); //B
//		v.x = x + sizeX; v.y = y;   v.z = z;   cubeVerts.push_back(v); //D
//		v.x = x + sizeX; v.y = y;   v.z = z;   cubeVerts.push_back(v); //D
//		v.x = x;   v.y = y;   v.z = z + sizeZ; cubeVerts.push_back(v); //B
//		v.x = x;   v.y = y;   v.z = z;   cubeVerts.push_back(v); //A
//		break;
//	case LEFT:
//		// Add a new face.
//		v.x = x;   v.y = y + sizeY; v.z = z + sizeZ; cubeVerts.push_back(v); //A
//		v.x = x;   v.y = y + sizeY; v.z = z;   cubeVerts.push_back(v); //B
//		v.x = x;   v.y = y;   v.z = z + sizeZ; cubeVerts.push_back(v); //D
//		v.x = x;   v.y = y;   v.z = z + sizeZ; cubeVerts.push_back(v); //D
//		v.x = x;   v.y = y + sizeY; v.z = z;   cubeVerts.push_back(v); //B
//		v.x = x;   v.y = y;   v.z = z;   cubeVerts.push_back(v); //C
//		break;
//	case RIGHT:
//		// Add a new face.
//		v.x = x + sizeX; v.y = y + sizeY; v.z = z + sizeZ; cubeVerts.push_back(v); //B
//		v.x = x + sizeX; v.y = y;   v.z = z + sizeZ; cubeVerts.push_back(v); //C
//		v.x = x + sizeX; v.y = y + sizeY; v.z = z;   cubeVerts.push_back(v); //A
//		v.x = x + sizeX; v.y = y + sizeY; v.z = z;   cubeVerts.push_back(v); //A
//		v.x = x + sizeX; v.y = y;   v.z = z + sizeZ; cubeVerts.push_back(v); //C
//		v.x = x + sizeX; v.y = y;   v.z = z;   cubeVerts.push_back(v); //D
//		break;
//	case FRONT:
//		// Add a new face.
//		v.x = x + sizeX; v.y = y + sizeY; v.z = z + sizeZ; cubeVerts.push_back(v); //A
//		v.x = x;   v.y = y + sizeY; v.z = z + sizeZ; cubeVerts.push_back(v); //B
//		v.x = x;   v.y = y;   v.z = z + sizeZ; cubeVerts.push_back(v); //C
//		v.x = x;   v.y = y;   v.z = z + sizeZ; cubeVerts.push_back(v); //C
//		v.x = x + sizeX; v.y = y;   v.z = z + sizeZ; cubeVerts.push_back(v); //D
//		v.x = x + sizeX; v.y = y + sizeY; v.z = z + sizeZ; cubeVerts.push_back(v); //A
//		break;
//	case BACK:
//		// Add a new face.
//		v.x = x;   v.y = y;   v.z = z;   cubeVerts.push_back(v); //D
//		v.x = x;   v.y = y + sizeY; v.z = z;   cubeVerts.push_back(v); //A
//		v.x = x + sizeX; v.y = y + sizeY; v.z = z;   cubeVerts.push_back(v); //B
//		v.x = x + sizeX; v.y = y + sizeY; v.z = z;   cubeVerts.push_back(v); //B
//		v.x = x + sizeX; v.y = y;   v.z = z;   cubeVerts.push_back(v); //C
//		v.x = x;   v.y = y;   v.z = z;   cubeVerts.push_back(v); //D
//		break;
//	default:
//		break;
//	}
//}
//
//void Mesher::shader(GLuint programID, GLuint cubeShader2, Camera *gcamera)
//{
//	float x;
//	float y;
//	float z;
//	float x2;
//	float y2;
//	float z2;
//
//	//gcamera->GetPos(x, y, z);
//	//gcamera->GetDirectionVector(x2, y2, z2);
//
//	// Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
//	glm::mat4 Projection = glm::perspective(90.0f, 640.0f / 480.0f, 0.f, 100.0f);
//	// Camera matrix
//	glm::mat4 View = glm::lookAt(
//		glm::vec3(x, y, z), // Camera is at (4,3,3), in World Space
//		glm::vec3(x + x2, y + y2, z + z2), // and looks at the origin
//		glm::vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
//		);
//	// Model matrix : an identity matrix (model will be at the origin)
//	glm::mat4 Model = glm::mat4(1.0f);  // Changes for each model !
//	// Our ModelViewProjection : multiplication of our 3 matrices
//	glm::mat4 MVP = Projection * View * Model; // Remember, matrix multiplication is the other way around
//
//	// Get a handle for our "MVP" uniform.
//	// Only at initialisation time.
//	GLuint MatrixID = glGetUniformLocation(programID, "MVP");
//	GLuint ViewMatrixID = glGetUniformLocation(programID, "V");
//	GLuint ModelMatrixID = glGetUniformLocation(programID, "M");
//	GLuint LightPowerID = glGetUniformLocation(programID, "LightPower");
//	GLuint LightID = glGetUniformLocation(programID, "LightPosition_worldspace");
//
//
//	//glBindTexture(GL_TEXTURE_2D, 0);
//	//######################
//
//	//GLuint v3CameraPos = glGetUniformLocation(cubeShader2, "v3CameraPos");
//	//GLuint v3LightPos = glGetUniformLocation(cubeShader2, "v3LightPos");
//	//GLuint v3InvWavelength = glGetUniformLocation(cubeShader2, "v3InvWavelength");
//	//GLuint fCameraHeight = glGetUniformLocation(cubeShader2, "fCameraHeight");
//	//GLuint fCameraHeight2 = glGetUniformLocation(cubeShader2, "fCameraHeight2");
//	//GLuint fInnerRadius = glGetUniformLocation(cubeShader2, "fInnerRadius");
//	//GLuint fInnerRadius2 = glGetUniformLocation(cubeShader2, "fInnerRadius2");
//	//GLuint fOuterRadius = glGetUniformLocation(cubeShader2, "fOuterRadius");
//	//GLuint fOuterRadius2 = glGetUniformLocation(cubeShader2, "fOuterRadius2");
//	//GLuint fKrESun = glGetUniformLocation(cubeShader2, "fKrESun");
//	//GLuint fKmESun = glGetUniformLocation(cubeShader2, "fKmESun");
//	//GLuint fKr4PI = glGetUniformLocation(cubeShader2, "fKr4PI");
//	//GLuint fKm4PI = glGetUniformLocation(cubeShader2, "fKm4PI");
//	//GLuint fScale = glGetUniformLocation(cubeShader2, "fScale");
//	//GLuint fScaleDepth = glGetUniformLocation(cubeShader2, "fScaleDepth");
//	//GLuint fScaleOverScaleDepth = glGetUniformLocation(cubeShader2, "fScaleOverScaleDepth");
//	//GLuint g = glGetUniformLocation(cubeShader2, "g");
//	//GLuint g2 = glGetUniformLocation(cubeShader2, "g2");
//	//GLuint s2Test = glGetUniformLocation(cubeShader2, "s2Test");
//
//
//	// Send our transformation to the currently bound shader, 
//	// in the "MVP" uniform
//	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
//	glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &Model[0][0]);
//	glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &View[0][0]);
//
//	glm::vec3 lightPos = glm::vec3(96 / 2, 16, 96 / 2);
//	glUniform3f(LightID, lightPos.x, lightPos.y, lightPos.z);
//	glUniform1f(LightPowerID, 5000);
//
//	//glUniform3f(v3CameraPos, gcamera.m_x, gcamera.m_y, gcamera.m_z);
//	//glm::vec3 lightDirection = glm::vec3(0, 0, 1) / glm::normalize(glm::vec3(0, 0, 1));
//	//glUniform3f(v3LightPos, lightDirection.x, lightDirection.y, lightDirection.z);
//	//glUniform3f(v3InvWavelength, 1.0f / pow(0.650f, 4.0f), 1.0f / pow(0.570f, 4.0f), 1.0f / pow(0.475f, 4.0f));
//	//glUniform1fARB(fCameraHeight, 10.0f + glm::length(gcamera.GetPosV3()));
//	//glUniform1fARB(fCameraHeight2, (10.0f + glm::length(gcamera.GetPosV3()))*(10.0f + glm::length(gcamera.GetPosV3())));
//	//glUniform1fARB(fInnerRadius, radin);
//	//glUniform1fARB(fInnerRadius2, radin*radin);
//	//glUniform1fARB(fOuterRadius, radout);
//	//glUniform1fARB(fOuterRadius2, radout * radout);
//	//glUniform1fARB(fKrESun, 0.0025f * 20.0f);
//	//glUniform1fARB(fKmESun, 0.0005f * 20.0f);
//	//glUniform1fARB(fKr4PI, 0.0025f * 4.0f * 3.141592653f);
//	//glUniform1fARB(fKm4PI, 0.0005f * 4.0f * 3.141592653f);
//	//glUniform1fARB(fScale, 1.0f / (radout - radin));
//	//glUniform1fARB(fScaleDepth, 0.25f);
//	//glUniform1fARB(fScaleOverScaleDepth, (1.0f / (radout - radin)) / 0.1f);
//	//glUniform1fARB(g, -0.990f);
//	//glUniform1f(g2, -0.990f * -0.990f);
//	//glUniform1f(s2Test, 0);
//
//	// Send our transformation to the currently bound shader,
//	// in the "MVP" uniform
//	// For each model you render, since the MVP will be different (at least the M part)
//	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
//}
//
//void Mesher::addChunk(int x2, int y2, int z2)
//{
//	for (int x = 0; x < 16; x++)
//	{
//		for (int y = 0; y < 16; y++)
//		{
//			for (int z = 0; z < 16; z++)
//			{
//				if (_coordSystem->getBlock(x + x2, y + y2, z + z2).x != NULL)
//				{
//					int sizeX = 1;
//					int sizeY = 1;
//					int sizeZ = 1;
//
//					if (_coordSystem->getBlock(x + x2, (y + y2) - 1, z + z2).x == 0)
//					{
//						addFace(x + x2, y + y2, z + z2, 2, sizeX, sizeY, sizeZ);
//					}
//
//					if (_coordSystem->getBlock(x + x2, (y + y2) + 1, z + z2).x == 0)
//					{
//						addFace(x + x2, y + y2, z + z2, 1, sizeX, sizeY, sizeZ);
//					}
//
//					if (_coordSystem->getBlock((x + x2) - 1, y + y2, z + z2).x == 0)
//					{
//						addFace(x + x2, y + y2, z + z2, 3, sizeX, sizeY, sizeZ);
//					}
//
//					if (_coordSystem->getBlock((x + x2) + 1, y + y2, z + z2).x == 0)
//					{
//						addFace(x + x2, y + y2, z + z2, 4, sizeX, sizeY, sizeZ);
//					}
//
//					if (_coordSystem->getBlock(x + x2, y + y2, (z + z2) - 1).x == 0)
//					{
//						addFace(x + x2, y + y2, z + z2, 6, sizeX, sizeY, sizeZ);
//					}
//
//					if (_coordSystem->getBlock(x + x2, y + y2, (z + z2) + 1).x == 0)
//					{
//						addFace(x + x2, y + y2, z + z2, 5, sizeX, sizeY, sizeZ);
//					}
//
//					addUV();
//					//addIndices();
//					addNormals();
//				}
//			}
//		}
//	}
//}
//
//void Mesher::fillChunk(int x, int y, int z)
//{
//	//for (int i = 0; i < 16; i++)
//	//{
//	//	for (int k = 0; k < 16; k++)
//	//	{
//	//		int hi = 16;
//	//		int lo = 0;
//
//	//		//float height = ((rand() % (hi - lo)) + lo);
//	//		//float height = 16 * glm::perlin(glm::vec4((i + x) / 16.f, (k + z) / 16.f, 0.5f, 0.5f));
//	//		//float height = 16 * glm::simplex(glm::vec4((i + x) / 16.f, (k + z) / 16.f, 0.5f, 0.5f));
//	//		//float height = 16 * glm::simplex(glm::vec3((i + x) / 16.f, (k + z) / 16.f, 0.5f));
//
//	//		float height = 16 * glm::perlin(glm::vec4((i + x) / 16.f, (k + z) / 16.f, 16.f, 0.f));
//	//		//float height = 16;
//
//	//		// Fill blocksArray with different value according to the height of the block
//	//		for (int j = 0; j < height; j++)
//	//		{
//	//			coordSystem->addCoords(i + x, j + y, k + z, 1);
//	//		}
//	//	}
//	//}
//
//	//addChunk(x, y, z);
//}
//
//void Mesher::createTerrain2()
//{
//	//	int viewDistance = 256;
//	//	int chunkDistance = viewDistance / 16;
//
//	//	//Planet::biomeLoader(0,0,0);
//
//		fillChunk(0, 0, 16);
//		fillChunk(0, 0, 32);
//		fillChunk(0, 0, 48);
//		fillChunk(0, 0, 64);
//		fillChunk(0, 0, 80);
//		fillChunk(0, 0, 96);
//
//		fillChunk(16, 0, 0);
//		fillChunk(16, 0, 16);
//		fillChunk(16, 0, 32);
//		fillChunk(16, 0, 48);
//		fillChunk(16, 0, 64);
//		fillChunk(16, 0, 80);
//		fillChunk(16, 0, 96);
//
//		fillChunk(32, 0, 0);
//		fillChunk(32, 0, 16);
//		fillChunk(32, 0, 32);
//		fillChunk(32, 0, 48);
//		fillChunk(32, 0, 64);
//		fillChunk(32, 0, 80);
//		fillChunk(32, 0, 96);
//
//		fillChunk(48, 0, 0);
//		fillChunk(48, 0, 16);
//		fillChunk(48, 0, 32);
//		fillChunk(48, 0, 48);
//		fillChunk(48, 0, 64);
//		fillChunk(48, 0, 80);
//		fillChunk(48, 0, 96);
//
//		fillChunk(64, 0, 0);
//		fillChunk(64, 0, 16);
//		fillChunk(64, 0, 32);
//		fillChunk(64, 0, 48);
//		fillChunk(64, 0, 64);
//		fillChunk(64, 0, 80);
//		fillChunk(64, 0, 96);
//
//		fillChunk(80, 0, 0);
//		fillChunk(80, 0, 16);
//		fillChunk(80, 0, 32);
//		fillChunk(80, 0, 48);
//		fillChunk(80, 0, 64);
//		fillChunk(80, 0, 80);
//		fillChunk(80, 0, 96);
//
//		fillChunk(96, 0, 0);
//		fillChunk(96, 0, 16);
//		fillChunk(96, 0, 32);
//		fillChunk(96, 0, 48);
//		fillChunk(96, 0, 64);
//		fillChunk(96, 0, 80);
//		fillChunk(96, 0, 96);
//
//		std::cout << voxelCount << std::endl;
//}
