#include <iostream>
#include <string>

using namespace std;

class DPackage
{
public:
	float float1 = 0;
	float float2 = 0;
	float float3 = 0;
	int blocktype1 = 0;
	int blockgridID = 0;
	std::string str1;
	std::string str2;
	std::string str3;
	char* char1 = 0;
	char* char2 = 0;
	char* char3 = 0;

	DPackage(float varf1, float varf2, float varf3, char* varc1, char* varc2, char* varc3, std::string varstr1, std::string varstr2, std::string varstr3);
	DPackage(float varf1, float varf2, float varf3);
	DPackage(float varf1, float varf2, float varf3, int blocktype, int bgID);
	DPackage(char* varc1, char* varc2, char* varc3);
	DPackage(std::string varstr1, std::string varstr2, std::string varstr3);
};