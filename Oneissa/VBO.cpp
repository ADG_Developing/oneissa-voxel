#include "VBO.h"

#include "Camera.h"
#include "OneissaMath.h"
#include "ContentSystem.h"
#include "BlockManager.h"
#include "Block.h"

VBO::VBO(OneissaMath* _omath, ContentSystem* _contsystem, BlockManager* _bMan)
{
	_oneissaMath = _omath;
	_contentSystem = _contsystem;
}

VBO::~VBO()
{

}

void VBO::initialize(int w, int h, int l)
{
	int cubeTotal = w * h * l;

	//cubeIndices = new GLuint[cubeTotal * 36];
	//cubeIndices.reserve(w*h*l*36);

	glGenBuffers(1, &vertexBuffer);
	glGenBuffers(1, &uvBuffer);
	glGenBuffers(1, &normalBuffer);
	glGenBuffers(1, &indexBuffer);

	glEnable(GL_TEXTURE_2D);

	texture[0] = _contentSystem->loadPNG(1, "../data/textures/blocks/terrain.png");
}

double vec3Distance(glm::vec3 vec1, glm::vec3 vec2)
{
	return sqrt((vec2.x - vec1.x)*(vec2.x - vec1.x) + (vec2.y - vec1.y)*(vec2.y - vec1.y) + (vec2.z - vec1.z)*(vec2.z - vec1.z));
}

void VBO::buildVBO()
{
	const int numIndices = (6 * cubeVerts.size()) / 4;

	//std::sort(&cubeVerts.begin(), &cubeVerts.end());

	//glm::vec3 distanceToCamera;
	//distanceToCamera = vec3Distance(glm::vec3, glm::vec3);

	//Init the buffer, once
	if (!cubeVerts.empty()) {
		//for (int e = 0; e < cubeVerts.size() - 1; e++)
		//{

		//}

		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, cubeVerts.size() * sizeof(glm::vec3), &cubeVerts[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
		glBufferData(GL_ARRAY_BUFFER, cubeNormals.size() * sizeof(glm::vec3), &cubeNormals[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
		glBufferData(GL_ARRAY_BUFFER, cubeUVs.size() * sizeof(glm::vec2), &cubeUVs[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(unsigned int), &cubeIndices[0], GL_STATIC_DRAW);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void VBO::shader(GLuint programID, GLuint cubeShader2, Camera *gcamera)
{
	//gcamera->GetPos(x, y, z);
	//gcamera->GetDirectionVector(x2, y2, z2);
	glm::vec3 pos = gcamera->getPosition();

	glm::mat4 Projection = glm::perspective(67.f, 1280.0f / 900.0f, 0.1f, 10000.0f);
	//glm::mat4 View = glm::lookAt(
	//	glm::vec3(pos.x, y, z), // Camera is at (4,3,3), in World Space
	//	glm::vec3(x + x2, y + y2, z + z2), // and looks at the origin
	//	glm::vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
	//	);

	// Model matrix : an identity matrix (model will be at the origin)
	glm::mat4 Model = glm::mat4(1.0f);  // Changes for each model !
	// Our ModelViewProjection : multiplication of our 3 matrices
	glm::mat4 MVP = Projection * gcamera->getViewMatrix() * Model; // Remember, matrix multiplication is the other way around

	glm::mat4 currMat(1.0f);
	currMat = glm::translate(currMat, glm::vec3(0.5f, 0.5f, 0.0f));
	currMat = glm::rotate(currMat, 90.0f, glm::vec3(0.0f, 0.0f, 1.0f));
	currMat = glm::translate(currMat, glm::vec3(-0.5f, -0.5f, 0.0f));

	// Get a handle for our "MVP" uniform.
	// Only at initialisation time.
	GLuint RotMatrixID = glGetUniformLocation(programID, "rotMatrix");
	GLuint MatrixID = glGetUniformLocation(programID, "MVP");
	GLuint ViewMatrixID = glGetUniformLocation(programID, "V");
	GLuint ModelMatrixID = glGetUniformLocation(programID, "M");
	GLuint LightPowerID = glGetUniformLocation(programID, "LightPower");
	GLuint LightID = glGetUniformLocation(programID, "LightPosition_worldspace");


	//glBindTexture(GL_TEXTURE_2D, 0);
	//######################

	//GLuint v3CameraPos = glGetUniformLocation(cubeShader2, "v3CameraPos");
	//GLuint v3LightPos = glGetUniformLocation(cubeShader2, "v3LightPos");
	//GLuint v3InvWavelength = glGetUniformLocation(cubeShader2, "v3InvWavelength");
	//GLuint fCameraHeight = glGetUniformLocation(cubeShader2, "fCameraHeight");
	//GLuint fCameraHeight2 = glGetUniformLocation(cubeShader2, "fCameraHeight2");
	//GLuint fInnerRadius = glGetUniformLocation(cubeShader2, "fInnerRadius");
	//GLuint fInnerRadius2 = glGetUniformLocation(cubeShader2, "fInnerRadius2");
	//GLuint fOuterRadius = glGetUniformLocation(cubeShader2, "fOuterRadius");
	//GLuint fOuterRadius2 = glGetUniformLocation(cubeShader2, "fOuterRadius2");
	//GLuint fKrESun = glGetUniformLocation(cubeShader2, "fKrESun");
	//GLuint fKmESun = glGetUniformLocation(cubeShader2, "fKmESun");
	//GLuint fKr4PI = glGetUniformLocation(cubeShader2, "fKr4PI");
	//GLuint fKm4PI = glGetUniformLocation(cubeShader2, "fKm4PI");
	//GLuint fScale = glGetUniformLocation(cubeShader2, "fScale");
	//GLuint fScaleDepth = glGetUniformLocation(cubeShader2, "fScaleDepth");
	//GLuint fScaleOverScaleDepth = glGetUniformLocation(cubeShader2, "fScaleOverScaleDepth");
	//GLuint g = glGetUniformLocation(cubeShader2, "g");
	//GLuint g2 = glGetUniformLocation(cubeShader2, "g2");
	//GLuint s2Test = glGetUniformLocation(cubeShader2, "s2Test");


	// Send our transformation to the currently bound shader, 
	// in the "MVP" uniform
	glUniformMatrix4fv(RotMatrixID, 1, GL_FALSE, &currMat[0][0]);
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
	glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &Model[0][0]);
	glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &gcamera->getViewMatrix()[0][0]);

	glm::vec3 lightPos = glm::vec3(0, 10000000000, 0);
	glUniform3f(LightID, lightPos.x, lightPos.y, lightPos.z);
	glUniform1f(LightPowerID, 1);

	//glUniform3f(v3CameraPos, gcamera.m_x, gcamera.m_y, gcamera.m_z);
	//glm::vec3 lightDirection = glm::vec3(0, 0, 1) / glm::normalize(glm::vec3(0, 0, 1));
	//glUniform3f(v3LightPos, lightDirection.x, lightDirection.y, lightDirection.z);
	//glUniform3f(v3InvWavelength, 1.0f / pow(0.650f, 4.0f), 1.0f / pow(0.570f, 4.0f), 1.0f / pow(0.475f, 4.0f));
	//glUniform1fARB(fCameraHeight, 10.0f + glm::length(gcamera.GetPosV3()));
	//glUniform1fARB(fCameraHeight2, (10.0f + glm::length(gcamera.GetPosV3()))*(10.0f + glm::length(gcamera.GetPosV3())));
	//glUniform1fARB(fInnerRadius, radin);
	//glUniform1fARB(fInnerRadius2, radin*radin);
	//glUniform1fARB(fOuterRadius, radout);
	//glUniform1fARB(fOuterRadius2, radout * radout);
	//glUniform1fARB(fKrESun, 0.0025f * 20.0f);
	//glUniform1fARB(fKmESun, 0.0005f * 20.0f);
	//glUniform1fARB(fKr4PI, 0.0025f * 4.0f * 3.141592653f);
	//glUniform1fARB(fKm4PI, 0.0005f * 4.0f * 3.141592653f);
	//glUniform1fARB(fScale, 1.0f / (radout - radin));
	//glUniform1fARB(fScaleDepth, 0.25f);
	//glUniform1fARB(fScaleOverScaleDepth, (1.0f / (radout - radin)) / 0.1f);
	//glUniform1fARB(g, -0.990f);
	//glUniform1f(g2, -0.990f * -0.990f);
	//glUniform1f(s2Test, 0);

	// Send our transformation to the currently bound shader,
	// in the "MVP" uniform
	// For each model you render, since the MVP will be different (at least the M part)
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
}

void VBO::renderVoxels()
{
	const int numIndices = (6 * cubeVerts.size()) / 4;

	glBindTexture(GL_TEXTURE_2D, texture[0]);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	// 3rd attribute buffer : normals
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
	glVertexAttribPointer(
		2,                                // attribute
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	// 2nd attribute buffer : UVs
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
	glVertexAttribPointer(
		1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		2,                                // size : U+V => 2
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// Index buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

	//glDrawArrays(GL_TRIANGLES, 0, cubeVerts.size());
	glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, (void*)0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
}

void VBO::addIndices(int w, int h, int l)
{
	int k = 0;
	for (int i = 0; i < w * h * l * 36; i += 6){

		//cubeIndices[i] = k;
		//cubeIndices[i + 1] = k + 1;
		//cubeIndices[i + 2] = k + 2;
		//cubeIndices[i + 3] = k + 2;
		//cubeIndices[i + 4] = k + 3;
		//cubeIndices[i + 5] = k;

		cubeIndices.insert(cubeIndices.begin() + i, k);
		cubeIndices.insert(cubeIndices.begin() + (i + 1), k + 1);
		cubeIndices.insert(cubeIndices.begin() + (i + 2), k + 2);
		cubeIndices.insert(cubeIndices.begin() + (i + 3), k + 2);
		cubeIndices.insert(cubeIndices.begin() + (i + 4), k + 3);
		cubeIndices.insert(cubeIndices.begin() + (i + 5), k);
		
		k += 4;
	}
}

void VBO::addIndices2()
{
	//cubeIndices[i] = k;
	//cubeIndices[i + 1] = k + 1;
	//cubeIndices[i + 2] = k + 2;
	//cubeIndices[i + 3] = k + 2;
	//cubeIndices[i + 4] = k + 3;
	//cubeIndices[i + 5] = k;

	cubeIndices.insert(cubeIndices.begin() + indiceCounter, indiceCounter2);
	cubeIndices.insert(cubeIndices.begin() + (indiceCounter + 1), indiceCounter2 + 1);
	cubeIndices.insert(cubeIndices.begin() + (indiceCounter + 2), indiceCounter2 + 2);
	cubeIndices.insert(cubeIndices.begin() + (indiceCounter + 3), indiceCounter2 + 2);
	cubeIndices.insert(cubeIndices.begin() + (indiceCounter + 4), indiceCounter2 + 3);
	cubeIndices.insert(cubeIndices.begin() + (indiceCounter + 5), indiceCounter2);

	indiceCounter += 6;
	indiceCounter2 += 4;
}

void VBO::addNormals(int facing)
{
	glm::vec3* v2 = new glm::vec3();

	enum f{ TOP = 1, BOTTOM = 2, LEFT = 3, RIGHT = 4, FRONT = 5, BACK = 6 };

	switch (facing)
	{
	case TOP:
		// Top
		v2->x = 0; v2->y = 1; v2->z = 0; cubeNormals.push_back(*v2);
		v2->x = 0; v2->y = 1; v2->z = 0; cubeNormals.push_back(*v2);
		v2->x = 0; v2->y = 1; v2->z = 0; cubeNormals.push_back(*v2);
		v2->x = 0; v2->y = 1; v2->z = 0; cubeNormals.push_back(*v2);
		//v2->x = 0; v2->y = 1; v2->z = 0; cubeNormals.push_back(*v2);
		//v2->x = 0; v2->y = 1; v2->z = 0; cubeNormals.push_back(*v2);
		break;
	case BOTTOM:
		// Bottom
		v2->x = 0; v2->y = -1; v2->z = 0; cubeNormals.push_back(*v2);
		v2->x = 0; v2->y = -1; v2->z = 0; cubeNormals.push_back(*v2);
		v2->x = 0; v2->y = -1; v2->z = 0; cubeNormals.push_back(*v2);
		v2->x = 0; v2->y = -1; v2->z = 0; cubeNormals.push_back(*v2);
		//v2->x = 0; v2->y = -1; v2->z = 0; cubeNormals.push_back(*v2);
		//v2->x = 0; v2->y = -1; v2->z = 0; cubeNormals.push_back(*v2);
		break;
	case LEFT:
		// Left
		v2->x = -1; v2->y = 0; v2->z = 0; cubeNormals.push_back(*v2);
		v2->x = -1; v2->y = 0; v2->z = 0; cubeNormals.push_back(*v2);
		v2->x = -1; v2->y = 0; v2->z = 0; cubeNormals.push_back(*v2);
		v2->x = -1; v2->y = 0; v2->z = 0; cubeNormals.push_back(*v2);
		//v2->x = -1; v2->y = 0; v2->z = 0; cubeNormals.push_back(*v2);
		//v2->x = -1; v2->y = 0; v2->z = 0; cubeNormals.push_back(*v2);
		break;
	case RIGHT:
		// Right
		v2->x = 1; v2->y = 0; v2->z = 0; cubeNormals.push_back(*v2);
		v2->x = 1; v2->y = 0; v2->z = 0; cubeNormals.push_back(*v2);
		v2->x = 1; v2->y = 0; v2->z = 0; cubeNormals.push_back(*v2);
		v2->x = 1; v2->y = 0; v2->z = 0; cubeNormals.push_back(*v2);
		//v2->x = 1; v2->y = 0; v2->z = 0; cubeNormals.push_back(*v2);
		//v2->x = 1; v2->y = 0; v2->z = 0; cubeNormals.push_back(*v2);
		break;
	case FRONT:
		// Front
		v2->x = 0; v2->y = 0; v2->z = 1; cubeNormals.push_back(*v2);
		v2->x = 0; v2->y = 0; v2->z = 1; cubeNormals.push_back(*v2);
		v2->x = 0; v2->y = 0; v2->z = 1; cubeNormals.push_back(*v2);
		v2->x = 0; v2->y = 0; v2->z = 1; cubeNormals.push_back(*v2);
		//v2->x = 0; v2->y = 0; v2->z = 1; cubeNormals.push_back(*v2);
		//v2->x = 0; v2->y = 0; v2->z = 1; cubeNormals.push_back(*v2);
		break;
	case BACK:
		// Back
		v2->x = 0; v2->y = 0; v2->z = -1; cubeNormals.push_back(*v2);
		v2->x = 0; v2->y = 0; v2->z = -1; cubeNormals.push_back(*v2);
		v2->x = 0; v2->y = 0; v2->z = -1; cubeNormals.push_back(*v2);
		v2->x = 0; v2->y = 0; v2->z = -1; cubeNormals.push_back(*v2);
		//v2->x = 0; v2->y = 0; v2->z = -1; cubeNormals.push_back(*v2);
		//v2->x = 0; v2->y = 0; v2->z = -1; cubeNormals.push_back(*v2);
		break;
	default:
		break;
	}

	delete v2;
}

void VBO::addUV(int facing, Block* block)
{
	glm::vec2* v = new glm::vec2();

	int startTopX = 0;
	int startTopY = 0;
	int startBottomX = 0;
	int startBottomY = 0;
	int startSideX = 0;
	int startSideY = 0;
	int endTopX = 0;
	int endTopY = 0;
	int endBottomX = 0;
	int endBottomY = 0;
	int endSideX = 0;
	int endSideY = 0;

	if (block->texCoordTopX > 0 && block->texCoordTopY > 0 && block->texCoordBottomX > 0 && block->texCoordBottomY > 0 && block->texCoordSideX > 0 && block->texCoordSideY > 0)
	{
		startTopX = block->texCoordTopX - 1;
		startTopY = block->texCoordTopY - 1;
		startBottomX = block->texCoordBottomX - 1;
		startBottomY = block->texCoordBottomY - 1;
		startSideX = block->texCoordSideX - 1;
		startSideY = block->texCoordSideY - 1;

		endTopX = block->texCoordTopX;
		endTopY = block->texCoordTopY;
		endBottomX = block->texCoordBottomX;
		endBottomY = block->texCoordBottomY;
		endSideX = block->texCoordSideX;
		endSideY = block->texCoordSideY;
	}
	else
	{
		startTopX = 0;
		startTopY = 0;
		startBottomX = 0;
		startBottomY = 0;
		startSideX = 0;
		startSideY = 0;
	}

	//v.x = _oneissaMath->calculateUV(startX, startY, 256, 256).x; v.y = _oneissaMath->calculateUV(endX, endY, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(startX, startY, 256, 256).x; v.y = _oneissaMath->calculateUV(startX, startY, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(endX, endY, 256, 256).x; v.y = _oneissaMath->calculateUV(startX, startY, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(endX, endY, 256, 256).x; v.y = _oneissaMath->calculateUV(endX, endY, 256, 256).y; cubeUVs.push_back(v);

	//v.x = _oneissaMath->calculateUV(startX, startY, 256, 256).x; v.y = _oneissaMath->calculateUV(endX, endY, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(startX, startY, 256, 256).x; v.y = _oneissaMath->calculateUV(startX, startY, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(endX, endY, 256, 256).x; v.y = _oneissaMath->calculateUV(startX, startY, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(endX, endY, 256, 256).x; v.y = _oneissaMath->calculateUV(endX, endY, 256, 256).y; cubeUVs.push_back(v);

	//v.x = _oneissaMath->calculateUV(startX, startY, 256, 256).x; v.y = _oneissaMath->calculateUV(endX, endY, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(startX, startY, 256, 256).x; v.y = _oneissaMath->calculateUV(startX, startY, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(endX, endY, 256, 256).x; v.y = _oneissaMath->calculateUV(startX, startY, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(endX, endY, 256, 256).x; v.y = _oneissaMath->calculateUV(endX, endY, 256, 256).y; cubeUVs.push_back(v);

	//v.x = _oneissaMath->calculateUV(startX, startY, 256, 256).x; v.y = _oneissaMath->calculateUV(endX, endY, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(startX, startY, 256, 256).x; v.y = _oneissaMath->calculateUV(startX, startY, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(endX, endY, 256, 256).x; v.y = _oneissaMath->calculateUV(startX, startY, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(endX, endY, 256, 256).x; v.y = _oneissaMath->calculateUV(endX, endY, 256, 256).y; cubeUVs.push_back(v);

	//v.x = _oneissaMath->calculateUV(startX, startY, 256, 256).x; v.y = _oneissaMath->calculateUV(endX, endY, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(startX, startY, 256, 256).x; v.y = _oneissaMath->calculateUV(startX, startY, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(endX, endY, 256, 256).x; v.y = _oneissaMath->calculateUV(startX, startY, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(endX, endY, 256, 256).x; v.y = _oneissaMath->calculateUV(endX, endY, 256, 256).y; cubeUVs.push_back(v);

	//v.x = _oneissaMath->calculateUV(startX, startY, 256, 256).x; v.y = _oneissaMath->calculateUV(endX, endY, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(startX, startY, 256, 256).x; v.y = _oneissaMath->calculateUV(startX, startY, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(endX, endY, 256, 256).x; v.y = _oneissaMath->calculateUV(startX, startY, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(endX, endY, 256, 256).x; v.y = _oneissaMath->calculateUV(endX, endY, 256, 256).y; cubeUVs.push_back(v);

	//v.x = _oneissaMath->calculateUV(0, 0, 256, 256).x; v.y = _oneissaMath->calculateUV(1, 1, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(0, 0, 256, 256).x; v.y = _oneissaMath->calculateUV(0, 0, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(1, 1, 256, 256).x; v.y = _oneissaMath->calculateUV(0, 0, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(1, 1, 256, 256).x; v.y = _oneissaMath->calculateUV(1, 1, 256, 256).y; cubeUVs.push_back(v);

	//v.x = _oneissaMath->calculateUV(2, 0, 256, 256).x; v.y = _oneissaMath->calculateUV(3, 1, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(2, 0, 256, 256).x; v.y = _oneissaMath->calculateUV(2, 0, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(3, 1, 256, 256).x; v.y = _oneissaMath->calculateUV(2, 0, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(3, 1, 256, 256).x; v.y = _oneissaMath->calculateUV(3, 1, 256, 256).y; cubeUVs.push_back(v);

	//v.x = _oneissaMath->calculateUV(3, 0, 256, 256).x; v.y = _oneissaMath->calculateUV(4, 1, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(3, 0, 256, 256).x; v.y = _oneissaMath->calculateUV(3, 0, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(4, 1, 256, 256).x; v.y = _oneissaMath->calculateUV(3, 0, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(4, 1, 256, 256).x; v.y = _oneissaMath->calculateUV(4, 1, 256, 256).y; cubeUVs.push_back(v);

	//v.x = _oneissaMath->calculateUV(3, 0, 256, 256).x; v.y = _oneissaMath->calculateUV(4, 1, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(3, 0, 256, 256).x; v.y = _oneissaMath->calculateUV(3, 0, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(4, 1, 256, 256).x; v.y = _oneissaMath->calculateUV(3, 0, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(4, 1, 256, 256).x; v.y = _oneissaMath->calculateUV(4, 1, 256, 256).y; cubeUVs.push_back(v);

	//v.x = _oneissaMath->calculateUV(3, 0, 256, 256).x; v.y = _oneissaMath->calculateUV(4, 1, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(3, 0, 256, 256).x; v.y = _oneissaMath->calculateUV(3, 0, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(4, 1, 256, 256).x; v.y = _oneissaMath->calculateUV(3, 0, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(4, 1, 256, 256).x; v.y = _oneissaMath->calculateUV(4, 1, 256, 256).y; cubeUVs.push_back(v);

	//v.x = _oneissaMath->calculateUV(3, 0, 256, 256).x; v.y = _oneissaMath->calculateUV(4, 1, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(3, 0, 256, 256).x; v.y = _oneissaMath->calculateUV(3, 0, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(4, 1, 256, 256).x; v.y = _oneissaMath->calculateUV(3, 0, 256, 256).y; cubeUVs.push_back(v);
	//v.x = _oneissaMath->calculateUV(4, 1, 256, 256).x; v.y = _oneissaMath->calculateUV(4, 1, 256, 256).y; cubeUVs.push_back(v);

	enum f{ TOP = 1, BOTTOM = 2, LEFT = 3, RIGHT = 4, FRONT = 5, BACK = 6 };

	switch (facing)
	{
	case TOP:
		v->x = _oneissaMath->calculateUV(startTopX, startTopY, 256, 256).x; v->y = _oneissaMath->calculateUV(endTopX, endTopY, 256, 256).y; cubeUVs.push_back(*v);
		v->x = _oneissaMath->calculateUV(startTopX, startTopY, 256, 256).x; v->y = _oneissaMath->calculateUV(startTopX, startTopY, 256, 256).y; cubeUVs.push_back(*v);
		v->x = _oneissaMath->calculateUV(endTopX, endTopY, 256, 256).x; v->y = _oneissaMath->calculateUV(startTopX, startTopY, 256, 256).y; cubeUVs.push_back(*v);
		v->x = _oneissaMath->calculateUV(endTopX, endTopY, 256, 256).x; v->y = _oneissaMath->calculateUV(endTopX, endTopY, 256, 256).y; cubeUVs.push_back(*v);
		break;
	case BOTTOM:
		v->x = _oneissaMath->calculateUV(startBottomX, startBottomY, 256, 256).x; v->y = _oneissaMath->calculateUV(endBottomX, endBottomY, 256, 256).y; cubeUVs.push_back(*v);
		v->x = _oneissaMath->calculateUV(startBottomX, startBottomY, 256, 256).x; v->y = _oneissaMath->calculateUV(startBottomX, startBottomY, 256, 256).y; cubeUVs.push_back(*v);
		v->x = _oneissaMath->calculateUV(endBottomX, endBottomY, 256, 256).x; v->y = _oneissaMath->calculateUV(startBottomX, startBottomY, 256, 256).y; cubeUVs.push_back(*v);
		v->x = _oneissaMath->calculateUV(endBottomX, endBottomY, 256, 256).x; v->y = _oneissaMath->calculateUV(endBottomX, endBottomY, 256, 256).y; cubeUVs.push_back(*v);
		break;
	case LEFT:
		v->x = _oneissaMath->calculateUV(startSideX, startSideY, 256, 256).x; v->y = _oneissaMath->calculateUV(startSideX, startSideY, 256, 256).y; cubeUVs.push_back(*v);
		v->x = _oneissaMath->calculateUV(endSideX, endSideY, 256, 256).x; v->y = _oneissaMath->calculateUV(startSideX, startSideY, 256, 256).y; cubeUVs.push_back(*v);
		v->x = _oneissaMath->calculateUV(endSideX, endSideY, 256, 256).x; v->y = _oneissaMath->calculateUV(endSideX, endSideY, 256, 256).y; cubeUVs.push_back(*v);
		v->x = _oneissaMath->calculateUV(startSideX, startSideY, 256, 256).x; v->y = _oneissaMath->calculateUV(endSideX, endSideY, 256, 256).y; cubeUVs.push_back(*v);
		break;
	case RIGHT:
		v->x = _oneissaMath->calculateUV(startSideX, startSideY, 256, 256).x; v->y = _oneissaMath->calculateUV(startSideX, startSideY, 256, 256).y; cubeUVs.push_back(*v);
		v->x = _oneissaMath->calculateUV(endSideX, endSideY, 256, 256).x; v->y = _oneissaMath->calculateUV(startSideX, startSideY, 256, 256).y; cubeUVs.push_back(*v);
		v->x = _oneissaMath->calculateUV(endSideX, endSideY, 256, 256).x; v->y = _oneissaMath->calculateUV(endSideX, endSideY, 256, 256).y; cubeUVs.push_back(*v);
		v->x = _oneissaMath->calculateUV(startSideX, startSideY, 256, 256).x; v->y = _oneissaMath->calculateUV(endSideX, endSideY, 256, 256).y; cubeUVs.push_back(*v);
		break;
	case FRONT:
		v->x = _oneissaMath->calculateUV(startSideX, startSideY, 256, 256).x; v->y = _oneissaMath->calculateUV(startSideX, startSideY, 256, 256).y; cubeUVs.push_back(*v);
		v->x = _oneissaMath->calculateUV(endSideX, endSideY, 256, 256).x; v->y = _oneissaMath->calculateUV(startSideX, startSideY, 256, 256).y; cubeUVs.push_back(*v);
		v->x = _oneissaMath->calculateUV(endSideX, endSideY, 256, 256).x; v->y = _oneissaMath->calculateUV(endSideX, endSideY, 256, 256).y; cubeUVs.push_back(*v);
		v->x = _oneissaMath->calculateUV(startSideX, startSideY, 256, 256).x; v->y = _oneissaMath->calculateUV(endSideX, endSideY, 256, 256).y; cubeUVs.push_back(*v);
		break;
	case BACK:
		v->x = _oneissaMath->calculateUV(startSideX, startSideY, 256, 256).x; v->y = _oneissaMath->calculateUV(startSideX, startSideY, 256, 256).y; cubeUVs.push_back(*v);
		v->x = _oneissaMath->calculateUV(endSideX, endSideY, 256, 256).x; v->y = _oneissaMath->calculateUV(startSideX, startSideY, 256, 256).y; cubeUVs.push_back(*v);
		v->x = _oneissaMath->calculateUV(endSideX, endSideY, 256, 256).x; v->y = _oneissaMath->calculateUV(endSideX, endSideY, 256, 256).y; cubeUVs.push_back(*v);
		v->x = _oneissaMath->calculateUV(startSideX, startSideY, 256, 256).x; v->y = _oneissaMath->calculateUV(endSideX, endSideY, 256, 256).y; cubeUVs.push_back(*v);
		break;
	default:
		break;
	}

	delete v;
}

void VBO::addVerts(float x, float y, float z, int facing, float w, float h, float l)
{
	glm::vec3* v = new glm::vec3();

	float sizeX = w;
	float sizeY = h;
	float sizeZ = l;

	enum f{ TOP = 1, BOTTOM = 2, LEFT = 3, RIGHT = 4, FRONT = 5, BACK = 6 };

	//switch (facing)
	//{
	//case TOP:
	//	// Add a new face.
	//	v.x = x;   v.y = y + sizeY; v.z = z;   cubeVerts.push_back(v); //A
	//	v.x = x;   v.y = y + sizeY; v.z = z + sizeZ; cubeVerts.push_back(v); //B
	//	v.x = x + sizeX; v.y = y + sizeY; v.z = z + sizeZ; cubeVerts.push_back(v); //C
	//	v.x = x + sizeX; v.y = y + sizeY; v.z = z + sizeZ; cubeVerts.push_back(v); //C
	//	v.x = x + sizeX; v.y = y + sizeY; v.z = z;   cubeVerts.push_back(v); //D
	//	v.x = x;   v.y = y + sizeY; v.z = z;   cubeVerts.push_back(v); //A
	//	break;
	//case BOTTOM:
	//	// Add a new face.
	//	v.x = x;   v.y = y;   v.z = z;   cubeVerts.push_back(v); //A
	//	v.x = x;   v.y = y;   v.z = z + sizeZ; cubeVerts.push_back(v); //B
	//	v.x = x + sizeX; v.y = y;   v.z = z + sizeZ; cubeVerts.push_back(v); //C
	//	v.x = x + sizeX; v.y = y;   v.z = z + sizeZ; cubeVerts.push_back(v); //C
	//	v.x = x + sizeX; v.y = y;   v.z = z;   cubeVerts.push_back(v); //D
	//	v.x = x;   v.y = y;   v.z = z;   cubeVerts.push_back(v); //A
	//	break;
	//case LEFT:
	//	// Add a new face.
	//	v.x = x;   v.y = y + sizeY; v.z = z + sizeZ; cubeVerts.push_back(v); //A
	//	v.x = x;   v.y = y + sizeY; v.z = z;   cubeVerts.push_back(v); //B
	//	v.x = x;   v.y = y;   v.z = z;   cubeVerts.push_back(v); //C
	//	v.x = x;   v.y = y;   v.z = z;   cubeVerts.push_back(v); //C
	//	v.x = x;   v.y = y;   v.z = z + sizeZ; cubeVerts.push_back(v); //D
	//	v.x = x;   v.y = y + sizeY; v.z = z + sizeZ; cubeVerts.push_back(v); //A
	//	break;
	//case RIGHT:
	//	// Add a new face.
	//	v.x = x + sizeX; v.y = y + sizeY; v.z = z;   cubeVerts.push_back(v); //A
	//	v.x = x + sizeX; v.y = y + sizeY; v.z = z + sizeZ; cubeVerts.push_back(v); //B
	//	v.x = x + sizeX; v.y = y;   v.z = z + sizeZ; cubeVerts.push_back(v); //C
	//	v.x = x + sizeX; v.y = y;   v.z = z + sizeZ; cubeVerts.push_back(v); //C
	//	v.x = x + sizeX; v.y = y;   v.z = z;   cubeVerts.push_back(v); //D
	//	v.x = x + sizeX; v.y = y + sizeY; v.z = z;   cubeVerts.push_back(v); //A
	//	break;
	//case FRONT:
	//	// Add a new face.
	//	v.x = x + sizeX; v.y = y + sizeY; v.z = z + sizeZ; cubeVerts.push_back(v); //A
	//	v.x = x;   v.y = y + sizeY; v.z = z + sizeZ; cubeVerts.push_back(v); //B
	//	v.x = x;   v.y = y;   v.z = z + sizeZ; cubeVerts.push_back(v); //C
	//	v.x = x;   v.y = y;   v.z = z + sizeZ; cubeVerts.push_back(v); //C
	//	v.x = x + sizeX; v.y = y;   v.z = z + sizeZ; cubeVerts.push_back(v); //D
	//	v.x = x + sizeX; v.y = y + sizeY; v.z = z + sizeZ; cubeVerts.push_back(v); //A
	//	break;
	//case BACK:
	//	// Add a new face.
	//	v.x = x;   v.y = y + sizeY; v.z = z;   cubeVerts.push_back(v); //A
	//	v.x = x + sizeX; v.y = y + sizeY; v.z = z;   cubeVerts.push_back(v); //B
	//	v.x = x + sizeX; v.y = y;   v.z = z;   cubeVerts.push_back(v); //C
	//	v.x = x + sizeX; v.y = y;   v.z = z;   cubeVerts.push_back(v); //C
	//    v.x = x;   v.y = y;   v.z = z;   cubeVerts.push_back(v); //D
	//	v.x = x;   v.y = y + sizeY; v.z = z;   cubeVerts.push_back(v); //A
	//	break;
	//default:
	//	break;
	//}

	switch (facing)
	{
	case TOP:
		// Add a new face.
		v->x = x;   v->y = y + sizeY; v->z = z;   cubeVerts.push_back(*v); //A
		v->x = x;   v->y = y + sizeY; v->z = z + sizeZ; cubeVerts.push_back(*v); //B
		v->x = x + sizeX; v->y = y + sizeY; v->z = z + sizeZ; cubeVerts.push_back(*v); //C 
		v->x = x + sizeX; v->y = y + sizeY; v->z = z;   cubeVerts.push_back(*v); //D

		//v->x = x;   v->y = y + sizeY; v->z = z;   cubeVerts.push_back(*v); //A
		//v->x = x;   v->y = y + sizeY; v->z = z + sizeZ; cubeVerts.push_back(*v); //B
		//v->x = x;   v->y = y + sizeY; v->z = z + sizeZ; cubeVerts.push_back(*v); //B 
		//v->x = x + sizeX; v->y = y + sizeY; v->z = z + sizeZ; cubeVerts.push_back(*v); //C 
		//v->x = x + sizeX; v->y = y + sizeY; v->z = z;   cubeVerts.push_back(*v); //D
		//v->x = x + sizeX; v->y = y + sizeY; v->z = z;   cubeVerts.push_back(*v); //D
		break;
	case BOTTOM:
		// Add a new face.
		v->x = x;   v->y = y;   v->z = z;   cubeVerts.push_back(*v); //A
		v->x = x;   v->y = y;   v->z = z + sizeZ; cubeVerts.push_back(*v); //B
		v->x = x + sizeX; v->y = y;   v->z = z + sizeZ; cubeVerts.push_back(*v); //C
		v->x = x + sizeX; v->y = y;   v->z = z;   cubeVerts.push_back(*v); //D

		//v->x = x + sizeX; v->y = y;   v->z = z + sizeZ; cubeVerts.push_back(*v); //C
		//v->x = x;   v->y = y;   v->z = z + sizeZ; cubeVerts.push_back(*v); //B
		//v->x = x + sizeX; v->y = y;   v->z = z;   cubeVerts.push_back(*v); //D
		//v->x = x + sizeX; v->y = y;   v->z = z;   cubeVerts.push_back(*v); //D
		//v->x = x;   v->y = y;   v->z = z + sizeZ; cubeVerts.push_back(*v); //B
		//v->x = x;   v->y = y;   v->z = z;   cubeVerts.push_back(*v); //A
		break;
	case LEFT:
		// Add a new face.
		v->x = x;   v->y = y + sizeY; v->z = z + sizeZ; cubeVerts.push_back(*v); //A
		v->x = x;   v->y = y + sizeY; v->z = z;   cubeVerts.push_back(*v); //B
		v->x = x;   v->y = y;   v->z = z;   cubeVerts.push_back(*v); //C
		v->x = x;   v->y = y;   v->z = z + sizeZ; cubeVerts.push_back(*v); //D

		//v->x = x;   v->y = y + sizeY; v->z = z + sizeZ; cubeVerts.push_back(*v); //A
		//v->x = x;   v->y = y + sizeY; v->z = z;   cubeVerts.push_back(*v); //B
		//v->x = x;   v->y = y + sizeY; v->z = z;   cubeVerts.push_back(*v); //B
		//v->x = x;   v->y = y;   v->z = z;   cubeVerts.push_back(*v); //C
		//v->x = x;   v->y = y;   v->z = z + sizeZ; cubeVerts.push_back(*v); //D
		//v->x = x;   v->y = y;   v->z = z + sizeZ; cubeVerts.push_back(*v); //D
		break;
	case RIGHT:
		// Add a new face.
		v->x = x + sizeX; v->y = y + sizeY; v->z = z;   cubeVerts.push_back(*v); //A
		v->x = x + sizeX; v->y = y + sizeY; v->z = z + sizeZ; cubeVerts.push_back(*v); //B
		v->x = x + sizeX; v->y = y;   v->z = z + sizeZ; cubeVerts.push_back(*v); //C
		v->x = x + sizeX; v->y = y;   v->z = z;   cubeVerts.push_back(*v); //D

		//v->x = x + sizeX; v->y = y + sizeY; v->z = z;   cubeVerts.push_back(*v); //A
		//v->x = x + sizeX; v->y = y + sizeY; v->z = z;   cubeVerts.push_back(*v); //A
		//v->x = x + sizeX; v->y = y + sizeY; v->z = z + sizeZ; cubeVerts.push_back(*v); //B
		//v->x = x + sizeX; v->y = y;   v->z = z + sizeZ; cubeVerts.push_back(*v); //C
		//v->x = x + sizeX; v->y = y;   v->z = z + sizeZ; cubeVerts.push_back(*v); //C
		//v->x = x + sizeX; v->y = y;   v->z = z;   cubeVerts.push_back(*v); //D
		break;
	case FRONT:
		// Add a new face.
		v->x = x + sizeX; v->y = y + sizeY; v->z = z + sizeZ; cubeVerts.push_back(*v); //A
		v->x = x;   v->y = y + sizeY; v->z = z + sizeZ; cubeVerts.push_back(*v); //B
		v->x = x;   v->y = y;   v->z = z + sizeZ; cubeVerts.push_back(*v); //C
		v->x = x + sizeX; v->y = y;   v->z = z + sizeZ; cubeVerts.push_back(*v); //D

		//v->x = x + sizeX; v->y = y + sizeY; v->z = z + sizeZ; cubeVerts.push_back(*v); //A
		//v->x = x + sizeX; v->y = y + sizeY; v->z = z + sizeZ; cubeVerts.push_back(*v); //A
		//v->x = x;   v->y = y + sizeY; v->z = z + sizeZ; cubeVerts.push_back(*v); //B
		//v->x = x;   v->y = y;   v->z = z + sizeZ; cubeVerts.push_back(*v); //C
		//v->x = x;   v->y = y;   v->z = z + sizeZ; cubeVerts.push_back(*v); //C
		//v->x = x + sizeX; v->y = y;   v->z = z + sizeZ; cubeVerts.push_back(*v); //D
		break;
	case BACK:
		// Add a new face.
		v->x = x;   v->y = y + sizeY; v->z = z;   cubeVerts.push_back(*v); //A
		v->x = x + sizeX; v->y = y + sizeY; v->z = z;   cubeVerts.push_back(*v); //B
		v->x = x + sizeX; v->y = y;   v->z = z;   cubeVerts.push_back(*v); //C
		v->x = x;   v->y = y;   v->z = z;   cubeVerts.push_back(*v); //D

		//v->x = x;   v->y = y + sizeY; v->z = z;   cubeVerts.push_back(*v); //A
		//v->x = x + sizeX; v->y = y + sizeY; v->z = z;   cubeVerts.push_back(*v); //B
		//v->x = x + sizeX; v->y = y + sizeY; v->z = z;   cubeVerts.push_back(*v); //B
		//v->x = x + sizeX; v->y = y;   v->z = z;   cubeVerts.push_back(*v); //C
		//v->x = x;   v->y = y;   v->z = z;   cubeVerts.push_back(*v); //D
		//v->x = x;   v->y = y;   v->z = z;   cubeVerts.push_back(*v); //D
		break;
	default:
		break;
	}

	delete v;
}